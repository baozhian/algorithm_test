package com.msb.student;

import org.junit.Test;

public class Code02 {


    // arr中，有两种数出现奇数次，其他数都出现了偶数次，找到这两种数
    // 一个数组中有一种数出现K次，其他数都出现了M次，找到出现了K次的数。
    @Test
    public void mainTest() {
        int[] arr = {9, 9, 9, 4, 4, 4, 4, 4, 3, 3, 3, 3, 3, 7, 7, 7, 7, 7};
        int i = onlyKTimes(arr, 3, 5);
        System.out.println(i);
    }


    public static int onlyKTimes(int[] arr, int k, int m) {
        // 新建一个固定的数组，存储每一位出现的次数
        int[] help = new int[32];
        for (int i = 0; i < arr.length; i++) {
            // 计算每个数在32位上的每个位上的值的累加
            for (int j = 0; j < help.length; j++) {
                help[j] += (arr[i] >> j) & 1;
            }
        }
        int ans = 0;
        for (int i = 0; i < help.length; i++) {
            if (help[i] == 0) {
                continue;
            }
            // 如果该位置上的累加和取余为K,那么说明出现K次的数在该位置上为1
            if (help[i] % m == k) {
                ans |= (1 << i);
            }
            // if (help[i] % m != 0) {
            //     ans |= (1 << i);
            // }
        }
        return ans;
    }

    //  head
    //   a    ->   b    ->  c  ->  null
    //  null  <-   a    <-  b  <-  c
    public static Node reverseLinkedList(Node head) {
        Node prev = null;
        while (head != null) {
            Node next = head.next;
            head.next = prev;
            prev = head;
            head = next;
        }
        return prev;
    }

    public static DoubleNode reverseDoubleList(DoubleNode head) {
        DoubleNode prev = null;
        while (head != null) {
            DoubleNode next = head.next;
            head.next = prev;
            head.last = next;
            prev = head;
            head = next;
        }
        return prev;
    }

    //  head
    //   a    ->   b    ->  c  ->  null
    //  null  <-   a    <-  b  <-  c
    public static Node reverseLinkedList(Node head, int value) {
        while (head != null) {
            if (head.value != value) {
                break;
            }
            head = head.next;
        }

        Node prev = head;
        Node curr = head;
        while (curr != null) {
            if (head.value == value) {
                prev.next = curr.next;
            } else {
                prev = curr;
            }
            curr = curr.next;
        }
        return head;
    }

}
