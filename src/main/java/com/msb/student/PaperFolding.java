package com.msb.student;

public class PaperFolding {

    public static void printAllFolds(int N) {
        printCurLevel(1, N, true);
    }

    // 当前你来了一个节点，脑海中想象的！
    // 这个节点在第i层，一共有N层，N固定不变的
    // 这个节点如果是凹的话，down = T
    // 这个节点如果是凸的话，down = F
    // 函数的功能：中序打印以你想象的节点为头的整棵树！
    private static void printCurLevel(int curLevel, int maxLevel, boolean isDown) {
        if (curLevel > maxLevel) {
            return;
        }
        printCurLevel(curLevel + 1, maxLevel, true);
        System.out.print(isDown ? "down " : "up ");
        printCurLevel(curLevel + 1, maxLevel, false);
    }

    public static void main(String[] args) {
        for (int n = 1; n < 5; n++) {
            printAllFolds(n);
            System.out.println();
        }
    }
}
