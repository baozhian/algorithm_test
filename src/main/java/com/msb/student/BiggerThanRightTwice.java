package com.msb.student;

public class BiggerThanRightTwice {
    public static void main(String[] args) {
        int[] data = {7, 8, 9, 23, 54, 5, 67, 2, 1, 10, 45, 3};
        System.out.println(test(data));
        System.out.println(process(data, 0, data.length - 1));
    }

    public static int process(int[] arr, int left, int right) {
        if (left == right) {
            return 0;
        }
        // left < right
        int mid = left + ((right - left) >> 1);
        return process(arr, left, mid)
                + process(arr, mid + 1, right)
                + merge(arr, left, mid, right);
    }

    public static int merge(int[] arr, int left, int mid, int right) {
        // [L....M] [M+1....R]
        int res = 0;
        // 目前囊括进来的数，是从[M+1, windowR)
        int windowR = mid + 1;
        for (int i = left; i <= mid; i++) {
            while (windowR <= right && arr[i] > (arr[windowR] << 1)) {
                windowR++;
            }
            res += windowR - mid - 1;
        }

        int[] help = new int[right - left + 1];
        int p1 = left;
        int p2 = mid + 1;
        int index = 0;
        while (p1 <= mid && p2 <= right) {
            help[index++] = arr[p1] <= arr[p2] ? arr[p1++] : arr[p2++];
        }
        while (p1 <= mid) {
            help[index++] = arr[p1++];
        }
        while (p2 <= right) {
            help[index++] = arr[p2++];
        }
        for (int i = 0; i < help.length; i++) {
            arr[left + i] = help[i];
        }
        return res;
    }

    // for test
    public static int test(int[] arr) {
        int ans = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] > (arr[j] << 1)) ans++;
            }
        }
        return ans;
    }
}
