package com.msb.student;

import java.util.Arrays;

public class HeapSort {

    public static void main(String[] args) {
        int[] data = new int[]{15, 2, 9, 6, 78, 45, 23, 1, 80, 67, 3};
        System.out.println(Arrays.toString(data));
        for (int i = 0; i < data.length; i++) {
            push(data, i);
        }
        System.out.println(Arrays.toString(data));

        for (int i = 0; i < data.length; i++) {
            pushV2(data, i);
        }
        System.out.println(Arrays.toString(data));
    }


    static void push(int[] data, int index) {
        while (data[index] > data[(index - 1) / 2]) {
            swap_v1(data, index, (index - 1) / 2);
            index = (index - 1) / 2;
        }
    }

    static void pushV2(int[] data, int index) {
        while (data[index] < data[(index - 1) / 2]) {
            swap_v1(data, index, (index - 1) / 2);
            index = (index - 1) / 2;
        }
    }

    static void swap_v1(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }
}
