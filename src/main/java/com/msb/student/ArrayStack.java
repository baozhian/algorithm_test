package com.msb.student;

public class ArrayStack {

    private final Integer[] data;

    private int index;

    private int size;


    public ArrayStack(int limit) {
        this.data = new Integer[limit];
        index = 0;
    }

    public void push(Integer value) {
        if (data.length == size) {
            throw new RuntimeException("栈已经满了");
        }
        data[index] = value;

        index++;
        size++;
    }

    public Integer pop() {
        if (size == 0) {
            throw new RuntimeException("栈已经空了");
        }
        int node = data[index - 1];
        data[index - 1] = null;
        index--;
        size--;
        return node;
    }

    public Integer peek() {
        if (size == 0) {
            throw new RuntimeException("栈已经空了");
        }
        return data[index - 1];
    }


    public static void main(String[] args) {
        ArrayStack arrayQueue = new ArrayStack(5);
        arrayQueue.push(4);
        arrayQueue.push(2);
        arrayQueue.push(3);
        arrayQueue.push(4);
        arrayQueue.push(5);
        System.out.println("size:" + arrayQueue.size);

        System.out.println(arrayQueue.peek());

        System.out.println("size:" + arrayQueue.size);

        System.out.println(arrayQueue.pop());
        System.out.println(arrayQueue.pop());
        System.out.println("size:" + arrayQueue.size);


        arrayQueue.push(5);
        System.out.println("size:" + arrayQueue.size);
    }
}
