package com.msb.student;

public class FindNode {

    public static class Node {
        Object value;
        Node left;
        Node right;
        Node parent;
    }

    public static Node getSuccessorNode(Node node) {
        if (node == null)
            return node;
        if (node.right != null) {
            return getLeftMost(node.left);
        }
        else {
            Node parent = node.parent;
            while (parent != null && parent.right == node) { // 当前节点是其父亲节点右孩子
                node = parent;
                parent = node.parent;
            }
            return parent;
        }
    }

    static Node getLeftMost(Node node) {
        if (node == null)
            return node;
        while (node.left != null)
            node = node.left;
        return node;
    }

    public static Node getPrecursorNode(Node node) {
        if (node == null)
            return node;
        if (node.left != null) {
            return getRightMost(node.left);
        }
        else {
            Node parent = node.parent;
            while (parent != null && parent.left == node) { // 当前节点是其父亲节点左孩子
                node = parent;
                parent = node.parent;
            }
            return parent;
        }
    }

    static Node getRightMost(Node node) {
        if (node == null)
            return node;
        while (node.right != null)
            node = node.right;
        return node;
    }
}