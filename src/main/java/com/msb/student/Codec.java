package com.msb.student;

import java.util.ArrayList;
import java.util.List;

public class Codec {

    // 提交时不要提交这个类
    public static class Node {
        public int val;
        public List<Node> children;

        public Node() {
        }

        public Node(int _val) {
            val = _val;
        }

        public Node(int _val, List<Node> _children) {
            val = _val;
            children = _children;
        }
    }

    ;

    // 提交时不要提交这个类
    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }


    public TreeNode encode(Node root) {
        if (root == null) {
            return null;
        }
        TreeNode head = new TreeNode(root.val);
        List<Node> children = root.children;
        if (children != null) {
            head.left = en(children);
        }
        return head;
    }

    // 让节点X的所有子节点放在左树的右边界
    TreeNode en(List<Node> children) {
        TreeNode head = null;
        TreeNode cur = null;
        for (Node child : children) {
            TreeNode treeNode = new TreeNode(child.val);
            if (head == null) {
                head = treeNode;
            } else {
                head.right = treeNode;
            }
            cur = treeNode;
            cur.left = encode(child);
        }
        return head;
    }

    // Decodes your binary tree to an n-ary tree.
    public Node decode(TreeNode root) {
        if (root == null) {
            return null;
        }
        Node head = new Node(root.val);
        if (root.left != null) {
            head.children = de(root.left);
        }
        return head;
    }

    List<Node> de(TreeNode root) {
        List<Node> objects = new ArrayList<>();
        TreeNode treeNode = root.right;
        while (treeNode != null) {
            Node node = new Node(treeNode.val, de(treeNode));
            objects.add(node);
            treeNode = treeNode.right;
        }
        return objects;
    }
}

