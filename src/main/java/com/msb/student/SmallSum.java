package com.msb.student;

public class SmallSum {
    public static void main(String[] args) {
        int[] data = {7, 8, 9, 23, 54, 5, 67, 2, 1, 10, 45, 3};
        System.out.println(test(data));
        System.out.println( mergeSort(data, 0, data.length - 1));
    }
    public static int mergeSort(int[] data, int left, int right) {
        if (left == right) {
            return 0;
        }
        int middle = left + ((right - left) >> 1);
        return mergeSort(data, left, middle)
                + mergeSort(data, middle + 1, right)
                + merge(data, left, middle, right);
    }

    private static int merge(int[] data, int left, int middle, int right) {
        int[] help = new int[right - left + 1];
        int index = 0;
        int p1 = left;
        int p2 = middle + 1;
        int res = 0;
        while (p1 <= middle && p2 <= right) {
            res += data[p1] < data[p2] ? (right - p2 + 1) * data[p1] : 0;
            help[index++] = data[p1] < data[p2] ? data[p1++] : data[p2++];
        }
        // 要么p1越界了，要么p2越界了
        while (p1 <= middle) {
            help[index++] = data[p1++];
        }
        while (p2 <= right) {
            help[index++] = data[p2++];
        }
        for (int i = 0; i < help.length; i++) {
            data[left + i] = help[i];
        }
        return res;
    }
    // 暴力测试
    private static int test(int[] data) {
        int res = 0;
        for (int i = data.length - 1; i >= 0; i--) {
            for (int j = 0; j < i; j++) {
                if (data[i] > data[j]) {
                    res += data[j];
                }
            }
        }
        return res;
    }
}
