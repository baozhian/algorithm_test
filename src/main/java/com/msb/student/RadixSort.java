package com.msb.student;

import java.util.Arrays;

public class RadixSort {

    public static void main(String[] args) {
        int[] data = {7, 8, 9, 23, 54, 5, 67, 2, 1, 10, 45, 3};
        System.out.println(Arrays.toString(data));
        radixSort(data, 0, data.length - 1, getMaxBits(data));
        System.out.println(Arrays.toString(data));
    }

    public static void radixSort(int[] data, int left, int right, int digit) {
        // 有多少个数准备多少个辅助空间
        int[] help = new int[data.length];
        int bit;
        for (int i = 0; i < digit; i++) {

            int[] count = new int[10]; // count[0..9]
            for (int n = 0; n < data.length; n++) {
                bit = getDigit(data[n], i + 1);
                count[bit]++;
            }
            for (int j = 1; j < count.length; j++) {
                count[j] = count[j] + count[j - 1];
            }

            for (int k = data.length - 1; k >= 0; k--) {
                bit = getDigit(data[k], i + 1);
                help[count[bit] - 1] = data[k];
                count[bit]--;
            }
            for (int x = 0; x < help.length; x++) {
                data[left + x] = help[x];
            }
        }
    }

    private static int getMaxBits(int[] data) {
        int max = data[0];
        for (int i = 1; i < data.length; i++) {
            max = Math.max(max, data[i]);
        }
        int res = 0;
        while (max != 0) {
            res++;
            max = max / 10;
        }
        return res;
    }

    private static int getDigit(int num, int bit) {
        return (num / (int) (Math.pow(10, bit - 1))) % 10;
    }
}
