package com.msb.student;

public class LinkedStack {

    private Node top;

    private int size;

    public void push(Integer value) {
        if (top == null) {
            top = new Node(value);
        } else {
            Node newTop = new Node(value);
            newTop.next = top;
            top = newTop;
        }
        size++;

    }

    public Integer pop() {
        if (top == null) {
            throw new RuntimeException("栈中已经空了");
        }
        Integer value = top.value;
        top = top.next;
        size--;
        return value;
    }

    public Integer peek() {
        if (top == null) {
            throw new RuntimeException("栈中已经空了");
        }
        return top.value;
    }

    public static void main(String[] args) {
        LinkedStack arrayQueue = new LinkedStack();
        arrayQueue.push(1);
        arrayQueue.push(2);
        arrayQueue.push(3);
        arrayQueue.push(4);
        arrayQueue.push(5);
        System.out.println(arrayQueue.size);  //5

        System.out.println(arrayQueue.peek()); //1

        System.out.println(arrayQueue.size); //5
        System.out.println(arrayQueue.pop()); //1
        System.out.println(arrayQueue.size); //4

        arrayQueue.push(5);
        arrayQueue.push(5);
        System.out.println(arrayQueue.size); //6

        System.out.println(arrayQueue.pop()); // 2

        System.out.println(arrayQueue.pop()); //3
        System.out.println(arrayQueue.pop()); //4
        System.out.println(arrayQueue.pop()); //5
        System.out.println(arrayQueue.pop()); //5
        System.out.println(arrayQueue.pop());
    }
}
