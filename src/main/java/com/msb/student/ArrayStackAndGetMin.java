package com.msb.student;

public class ArrayStackAndGetMin {
    private final Integer[] data;

    private final Integer[] minData;

    private int index;

    private int size;


    public ArrayStackAndGetMin(int limit) {
        this.data = new Integer[limit];
        this.minData = new Integer[limit];
        index = 0;
    }

    public void push(Integer value) {
        if (data.length == size) {
            throw new RuntimeException("栈已经满了");
        }
        Integer min = value;
        if (size > 0) {
            Integer peek = getMin();
            min = Math.min(min, peek);
        }
        data[index] = value;
        minData[index] = min;

        index++;
        size++;
    }

    public Integer pop() {
        if (size == 0) {
            throw new RuntimeException("栈已经空了");
        }
        int node = data[index - 1];
        data[index - 1] = null;

        minData[index - 1] = null;

        index--;
        size--;
        return node;
    }

    public Integer peek() {
        if (size == 0) {
            throw new RuntimeException("栈已经空了");
        }
        return data[index - 1];
    }


    public Integer getMin() {
        if (size == 0) {
            throw new RuntimeException("栈已经空了");
        }
        return minData[index - 1];
    }

    public static void main(String[] args) {
        ArrayStackAndGetMin arrayQueue = new ArrayStackAndGetMin(5);
        arrayQueue.push(4);
        arrayQueue.push(2);
        arrayQueue.push(3);
        arrayQueue.push(4);
        arrayQueue.push(5);
        System.out.println("size:" + arrayQueue.size);
        System.out.println("min:" + arrayQueue.getMin());
        System.out.println(arrayQueue.peek());

        System.out.println("size:" + arrayQueue.size);
        System.out.println("min:" + arrayQueue.getMin());
        System.out.println(arrayQueue.pop());
        System.out.println(arrayQueue.pop());
        System.out.println("size:" + arrayQueue.size);
        System.out.println("min:" + arrayQueue.getMin());

        arrayQueue.push(5);
        System.out.println("size:" + arrayQueue.size);
        System.out.println("min:" + arrayQueue.getMin());
    }
}
