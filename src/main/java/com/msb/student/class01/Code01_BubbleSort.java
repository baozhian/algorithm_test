package com.msb.student.class01;

import java.util.Arrays;

public class Code01_BubbleSort {

    public static void main(String[] args) {
        int[] data = {1, 7, 5, 9, 3, 2, 7};
        bubbleSort(data);
        System.out.println(Arrays.toString(data));
    }

    public static void bubbleSort(int[] data) {
        for (int i = data.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (data[j] > data[j + 1]) {
                    swap(j, j + 1, data);
                }
            }
        }
    }

    private static void swap(int i, int j, int[] data) {
        int tmp = data[i];
        data[i] = data[j];
        data[j] = tmp;
    }
}
