package com.msb.student;

import java.util.Arrays;

public class QuickSort {

    public static void main(String[] args) {
        int[] data = {7, 8, 9, 23, 54, 5, 67, 2, 1, 10, 45, 3};
        System.out.println(Arrays.toString(data));
        quickSort(data, 0, data.length - 1);
        System.out.println(Arrays.toString(data));
    }

    public void quickSort(int[] arr) {
        quickSort(arr, 0, arr.length - 1);
    }

    private static void quickSort(int[] arr, int left, int right) {
        //递归结束条件
        if (right <= left) {
            return;
        }
        // 随机确定出基准数，放在最右边，只用来比较，不做排序处理
        int pivot = left + (int) (Math.random() * (right - left + 1));
        swap_v1(arr, pivot, right);

        int mid = partition(arr, left, right);
        //递归调用，将左半部分排好序
        quickSort(arr, left, mid - 1);
        //递归调用，将由半部分排好序
        quickSort(arr, mid + 1, right);
    }

    // arr[L..R]上，以arr[R]位置的数做划分值
    // <= X > X
    // <= X X
    private static int partition(int[] arr, int left, int right) {
        if (left > right)
            return -1;
        if (left == right)
            return left;
        int lessEqual = left - 1;
        int cur = left;
        while (cur < right) {
            // arr[right]为基准值
            if (arr[cur] <= arr[right] && (++lessEqual) != cur)
                swap_v1(arr, lessEqual, cur);
            cur++;
        }
        // 基准值换到中间位置
        swap_v1(arr, ++lessEqual, right);
        return lessEqual;
    }


    public static void swap_v1(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }
}
