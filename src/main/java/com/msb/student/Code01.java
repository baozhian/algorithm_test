package com.msb.student;


import java.util.Arrays;

public class Code01 {

    // for test
    public static int[] generateRandomArray(int maxSize, int maxValue) {
        // Math.random() -> [0,1) 所有的小数，等概率返回一个
        // Math.random() * N -> [0,N) 所有小数，等概率返回一个
        // (int)(Math.random() * N) -> [0,N-1] 所有的整数，等概率返回一个
        int[] arr = new int[(int) ((maxSize + 1) * Math.random())]; // 长度随机
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) ((maxValue + 1) * Math.random()) - (int) (maxValue * Math.random());
        }
        return arr;
    }

    public static void swap_v2(int[] arr, int i, int j) {
        arr[i] = arr[i] ^ arr[j];
        arr[j] = arr[i] ^ arr[j];
        arr[i] = arr[i] ^ arr[j];
    }

    public static void swap_v1(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }


    public void mainTest() {
        int testTime = 50000;
        int maxSize = 30; // 随机数组的长度0～100
        int maxValue = 100;// 值：-100～100
        boolean succeed = true;
        for (int i = 0; i < testTime; i++) {
            int[] arr = generateRandomArray(maxSize, maxValue);
            int[] arr1 = Arrays.copyOf(arr, arr.length);
            int[] arr2 = Arrays.copyOf(arr, arr.length);
            quickSort(arr1);
            Arrays.sort(arr2);
            if (!Arrays.equals(arr1, arr2)) {
                // 打印arr1
                // 打印arr2
                succeed = false;
                for (int j = 0; j < arr.length; j++) {
                    System.out.print(arr[j] + " ");
                }
                System.out.println();
                break;
            }
        }
        System.out.println(succeed ? "Nice!" : "Fucking fucked!");

        int[] arr = generateRandomArray(maxSize, maxValue);
        System.out.println(Arrays.toString(arr));
        quickSort(arr);
        System.out.println(Arrays.toString(arr));
    }


    public void bubbleSort(int[] arr) {
        // 0 ~ N-1
        // 0 ~ N-2
        // 0 ~ N-3
        for (int i = arr.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arr[j] > arr[j + 1]) {
                    swap_v1(arr, j, j + 1);
                }
            }
        }
    }

    public void selectionSort(int[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[minIndex] > arr[j]) {
                    minIndex = j;
                }
            }
            swap_v1(arr, i, minIndex);
        }
    }


    public void insertionSort(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            for (int j = i - 1; j >= 0; j--) {
                if (arr[j] > arr[j + 1]) {
                    swap_v1(arr, j, j + 1);
                }
            }

        }
    }

    // 希尔排序
    public void shellSort(int[] arr) {
        // 遍历所有的步长
        int gap = arr.length / 2;
        while (gap > 0) {
            for (int i = gap; i < arr.length; i++) {
                //遍历本组中所有元素
                for (int j = i - gap; j >= 0; j -= gap) {
                    //如果当前元素大于加上步长后的那个元素
                    if (arr[j] > arr[j + gap]) {
                        swap_v1(arr, j, j + gap);
                    }
                }
            }
            gap = gap / 2;
        }
    }


    public void quickSort(int[] arr) {
        quickSort(arr, 0, arr.length - 1);
    }

    private static void quickSort(int[] arr, int left, int right) {
        //递归结束条件
        if (right < left) {
            return;
        }
        //定义两个遍历存放初始的left和right方便后面归为
        int low = left;
        int high = right;
        //确定出基准数
        int pivot = arr[low];
        //排序
        while (left != right) {
            //2.1、从右边开始找比基准数小的
            while (arr[right] >= pivot && right > left) {
                right--;
            }
            //2.2、从左边开始找比基准数大的
            while (arr[left] <= pivot && right > left) {
                left++;
            }
            //2.3、交换两个值得位置
            swap_v1(arr, left, right);
        }
        // 将原来的基准数调整到左边靠近中间，这个数后续位置将不再改变
        swap_v1(arr, left, low);
        //递归调用，将左半部分排好序
        quickSort(arr, low, left - 1);
        //递归调用，将由半部分排好序
        quickSort(arr, low + 1, high);
    }
}
