package com.msb.student;

import java.util.Arrays;

public class MergeSort {

    public static void main(String[] args) {
        int[] data = {7, 8, 9, 23, 54, 5, 67, 2, 1, 10, 45, 3};
        System.out.println(Arrays.toString(data));
        mergeSort(data, 0, data.length - 1);
        System.out.println(Arrays.toString(data));
    }

    public static void mergeSort(int[] data, int left, int right) {
        if (left == right) {
            return;
        }
        int middle = left + ((right - left) >> 1);
        mergeSort(data, left, middle);
        mergeSort(data, middle + 1, right);

        merge(data, left, middle, right);
    }

    private static void merge(int[] data, int left, int middle, int right) {
        int[] help = new int[right - left + 1];
        int index = 0;
        int p1 = left;
        int p2 = middle + 1;
        while (p1 <= middle && p2 <= right) {
            help[index++] = data[p1] <= data[p2] ? data[p1++] : data[p2++];
        }
        // 要么p1越界了，要么p2越界了
        while (p1 <= middle) {
            help[index++] = data[p1++];
        }
        while (p2 <= right) {
            help[index++] = data[p2++];
        }
        for (int i = 0; i < help.length; i++) {
            data[left + i] = help[i];
        }
    }
}
