package com.msb.student;

public class LinkedQueue {

    private Node head;

    private Node last;

    private int size;

    public void push(Integer value) {
        if (head == null) {
            head = new Node(value);
            last = head;
        } else {
            last.next = new Node(value);
            last = last.next;
        }
        size++;

    }

    public Integer pop() {
        if (head == null) {
            throw new RuntimeException("栈中已经空了");
        }
        Integer value = head.value;
        head = head.next;
        size--;
        return value;
    }

    public Integer peek() {
        if (head == null) {
            throw new RuntimeException("栈中已经空了");
        }
        return head.value;
    }

    public static void main(String[] args) {
        LinkedQueue arrayQueue = new LinkedQueue();
        arrayQueue.push(1);
        arrayQueue.push(2);
        arrayQueue.push(3);
        arrayQueue.push(4);
        arrayQueue.push(5);
        System.out.println(arrayQueue.size);  //5

        System.out.println(arrayQueue.peek()); //1

        System.out.println(arrayQueue.size); //5
        System.out.println(arrayQueue.pop()); //1
        System.out.println(arrayQueue.size); //4

        arrayQueue.push(5);
        arrayQueue.push(5);
        System.out.println(arrayQueue.size); //6

        System.out.println(arrayQueue.pop()); // 2

        System.out.println(arrayQueue.pop()); //3
        System.out.println(arrayQueue.pop()); //4
        System.out.println(arrayQueue.pop()); //5
        System.out.println(arrayQueue.pop()); //5
        System.out.println(arrayQueue.pop());
    }
}
