package com.msb.student;

public class RingArrayQueue {

    private final Integer[] data;

    private int start;

    private int end;

    private int size;

    public RingArrayQueue(int limit) {
        this.data = new Integer[limit];
        start = 0;
        end = 0;
    }

    public void push(int value) {
        if (data.length == size) {
            throw new RuntimeException("队列已经满了");
        }
        data[end] = value;
        end = setNextIndex(end);
        size++;
    }

    public Integer pop() {
        if (size == 0) {
            throw new RuntimeException("队列已经空了");
        }
        int node = data[start];
        data[start] = null;
        start = setNextIndex(start);
        size--;
        return node;
    }

    public Integer peek() {
        if (size == 0) {
            throw new RuntimeException("队列已经空了");
        }
        return data[start];
    }

    public int setNextIndex(int index) {
        return index < data.length - 1 ? index + 1 : 0;
    }

    public static void main(String[] args) {
        RingArrayQueue arrayQueue = new RingArrayQueue(5);
        arrayQueue.push(1);
        arrayQueue.push(2);
        arrayQueue.push(3);
        arrayQueue.push(4);
        arrayQueue.push(5);
        System.out.println(arrayQueue.size);

        System.out.println(arrayQueue.peek());

        System.out.println(arrayQueue.size);
        System.out.println(arrayQueue.pop());
        System.out.println(arrayQueue.size);

        arrayQueue.push(5);
        System.out.println(arrayQueue.size);
    }
}
