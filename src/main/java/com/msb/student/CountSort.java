package com.msb.student;

import java.util.Arrays;

public class CountSort {

    public static void main(String[] args) {
        int[] arr = {1, 4, 9, 2, 5, 3, 7, 6, 22, 23, 15, 24, 0,
                3, 4, 5, 2, 3, 5, 12, 1, 3, 4, 2, 1,
                3, 45, 1, 1};
        System.out.println(Arrays.toString(arr));
        // 计数排序
        countSort(arr);
        System.out.println(Arrays.toString(arr));
    }

    private static void countSort(int[] arr) {
        // 检测数据
        if (arr == null || arr.length == 0) {
            return ;
        }
        // 缩短不必要的长度，获取最大和最小值
        int min = arr[0];
        int max = arr[0];
        for (int i = 0; i < arr.length; i++) {
            max = Math.max(arr[i], max);
            min = Math.min(arr[i], min);
        }
        // 用数组中的值作为索引，个数作为值
        int[] help = new int[max - min + 1];
        for (int i = 0; i < arr.length; i++) {
            help[arr[i] - min]++;
        }
        // 反向遍历
        int k = 0;
        for (int i = 0; i < help.length; i++) {
            // 排列一个后，就减少一个
            while (help[i]-- > 0) {
                arr[k++] = i + min;
            }
        }
    }

}