package com.msb.student;

import java.util.*;
import java.util.stream.Collectors;

public class LineSegmentOverlap {

    public static void main(String[] args) {
        LineSegment[] data = {
                new LineSegment(1, 55),
                new LineSegment(2, 88),
                new LineSegment(50, 99),
                new LineSegment(10, 20),
                new LineSegment(50, 77),
                new LineSegment(45, 60)
        };
        List<LineSegment> segmentList = Arrays.stream(data)
                .collect(Collectors.toList());
        PriorityQueue<LineSegment> sortExtend = new PriorityQueue<>(
                Comparator.comparing(LineSegment::getStart));
        sortExtend.addAll(segmentList);

        PriorityQueue<Integer> heapSortExtend = new PriorityQueue<>();
        int max = 0;
        for (LineSegment segment : sortExtend) {
            int start = segment.getStart();
            Iterator<Integer> iterator = heapSortExtend.iterator();
            while (iterator.hasNext() && iterator.next() <= start) {
                iterator.remove();
            }
            int end = segment.end;
            heapSortExtend.add(end);
            max = Math.max(max, heapSortExtend.size());
        }
        System.out.println(max);

        List<LineSegment> lineSegments = Arrays.asList(
                new LineSegment(1, 55),
                new LineSegment(2, 88),
                new LineSegment(50, 99),
                new LineSegment(10, 20),
                new LineSegment(50, 77),
                new LineSegment(45, 60));
        System.out.println(maxCover(lineSegments));
    }

    private static class LineSegment {
        int start;
        int end;
        public LineSegment(int start, int end) {
            this.start = start;
            this.end = end;
        }
        public int getStart() {
            return start;
        }
    }

    public static int maxCover(List<LineSegment> lines) {
        lines.sort(Comparator.comparing(LineSegment::getStart));
        // 小根堆，每一条线段的结尾数值，使用默认的
        PriorityQueue<Integer> heap = new PriorityQueue<>();
        int max = 0;
        for (int i = 0; i < lines.size(); i++) {
            // lines[i] -> cur 在黑盒中，把<=cur.start 东西都弹出
            while (!heap.isEmpty() && heap.peek() <= lines.get(i).start) {
                heap.poll();
            }
            heap.add(lines.get(i).end);
            max = Math.max(max, heap.size());
        }
        return max;
    }
}
