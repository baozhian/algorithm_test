package com.msb.student;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class HeapSortExtend<T> {
    public static void main(String[] args) {
        int[] data = new int[]{15, 2, 9, 6, 78, 45, 23, 1, 80, 67, 3};
        System.out.println(Arrays.toString(data));

        ArrayList<Integer> integers = new ArrayList<>(15);
        HeapSortExtend<Integer> sortExtend = new HeapSortExtend<>(integers,
                Comparator.comparing(Integer::intValue));
        for (int datum : data) {
            sortExtend.push(datum);
        }
        System.out.println(sortExtend.data.toString());

        System.out.println(sortExtend.pop());
        System.out.println(sortExtend.data.toString());
        System.out.println(sortExtend.pop());
        System.out.println(sortExtend.data.toString());
        integers.clear();
        HeapSortExtend<Integer> sortExtend1 = new HeapSortExtend<>(integers,
                Comparator.comparing(Integer::intValue).reversed());
        for (int datum : data) {
            sortExtend1.push(datum);
        }
        System.out.println(sortExtend1.data.toString());

        System.out.println(sortExtend1.pop());
        System.out.println(sortExtend1.data.toString());
        System.out.println(sortExtend1.pop());
        System.out.println(sortExtend1.data.toString());
    }

    List<T> data;
    int size;
    Comparator<T> comparator;

    HeapSortExtend(List<T> data, Comparator<T> comparator) {
        this.data = data;
        this.comparator = comparator;
    }

    void push(T value) {
        int index = size;
        data.add(index, value);
        while (index >= 0 && comparator.compare(data.get(index), data.get((index - 1) / 2)) > 0) {
            swap(data, (index - 1) / 2, index);
            index = (index - 1) / 2;
        }
        size++;
    }

    T pop() {
        int index = 0;
        T result = data.get(index);
        int last = size - 1;
        T t = data.get(last);

        data.set(index, t);
        data.set(--size, null);
        if (size != 0 && index < size) {
            int left;
            while ((left = (2 * index + 1)) < size) {
                int large = left + 1 < size && comparator.compare(data.get(left + 1), data.get(left)) > 0 ?
                        left + 1 : left;
                large = comparator.compare(data.get(index), data.get(large)) > 0 ? index : large;
                if (index == large) {
                    break;
                }
                swap(data, large, index);
                index = large;
            }
        }
        return result;
    }

    void swap(List<T> data, int prev, int next) {
        T t = data.get(prev);
        T t1 = data.get(next);
        data.set(next, t);
        data.set(prev, t1);
    }
}
