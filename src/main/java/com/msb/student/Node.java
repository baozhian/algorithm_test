package com.msb.student;

public class Node {
    public Integer value;
    public Node next;

    public Node(Integer data) {
        value = data;
    }

    public static Node deleteValuesOrNode(Node head, int value) {
        while (head != null) {
            if (head.value != value) {
                break;
            }
            head = head.next;
        }
        Node prev = head;
        Node curr = head;
        while (curr != null) {
            if (head.value == value) {
                prev.next = curr.next;
            } else {
                prev = curr;
            }
            curr = curr.next;
        }
        return head;
    }

    public static Node deleteCurNode(Node delNode) {
        Node next = delNode.next;
        if (next != null) {
            delNode.value = next.value;
            delNode.next = next.next;
            next = null;
        } else {
            throw new RuntimeException("不支持最后一个节点删除");
        }
        return delNode;
    }
}

