package com.msb.trie;

/**
 * @author bza625858
 * @version : Trie.java, v 0.1 2024-06-22 13:35 bza625858 Exp $$
 */
public interface Trie {

    void insert(String word);

    void delete(String word);

    int searchCount(String word);

    int prefixNum(String prefix);
}
