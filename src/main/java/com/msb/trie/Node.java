package com.msb.trie;

public class Node {
    public int pass;
    public int end;
    public Node[] nexts;

    // char tmp = 'b'  (tmp - 'a')
    public Node() {
        pass = 0;
        end = 0;
        nexts = new Node[26];
    }
}