package com.msb.trie;

/**
 * @author bza625858
 * @version : TrieTree.java, v 0.1 2024-06-22 13:37 bza625858 Exp $$
 */
public class TrieTree implements Trie {

    private Node root;

    public TrieTree() {
        this.root = new Node();
    }

    @Override
    public void insert(String word) {
        if (word == null) {
            return;
        }
        Node node = this.root;
        node.pass++;
        char[] chars = word.toCharArray();
        for (char charStr : chars) {
            int path = charStr - 'a';
            if (node.nexts[path] == null) {
                node.nexts[path] = new Node();
            }
            node = node.nexts[path];
            node.pass++;
        }
        node.end++;
    }

    @Override
    public void delete(String word) {
        if (word == null) {
            return;
        }
        if (searchCount(word) == 0) {
            return;
        }
        Node node = this.root;
        node.pass--;
        char[] chars = word.toCharArray();
        for (char charStr : chars) {
            int path = charStr - 'a';
            if (--node.nexts[path].pass == 0) {
                node.nexts[path] = null;
                return;
            }
            node = node.nexts[path];
        }
        node.end--;
    }

    @Override
    public int searchCount(String word) {
        if (word == null) {
            return 0;
        }
        Node node = this.root;
        char[] chars = word.toCharArray();
        for (char charStr : chars) {
            int path = charStr - 'a';
            if (node.nexts[path] == null) {
                return 0;
            }
            node = node.nexts[path];
        }
        return node.end;
    }

    @Override
    public int prefixNum(String prefix) {
        if (prefix == null) {
            return 0;
        }
        Node node = this.root;
        char[] chars = prefix.toCharArray();
        for (char charStr : chars) {
            int path = charStr - 'a';
            if (node.nexts[path] == null) {
                return 0;
            }
            node = node.nexts[path];
        }
        return node.pass;
    }
}
