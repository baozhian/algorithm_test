package com.msb.dynamicprogram;

/**
 * @author bza625858
 * @version : ConvertToLetterString.java, v 0.1 2024-07-30 21:45 bza625858 Exp $$
 */
public class ConvertToLetterString {

    public static void main(String[] args) {
        System.out.println(getWays("12345"));
        System.out.println(getWaysByDp("12345"));
    }

    public static int getWays(String str) {
        if (str == null || str.length() == 0) {
            return 0;
        }
        return process(str.toCharArray(), 0);
    }

    private static int process(char[] chars, int i) {
        if (i == chars.length) {
            return 1;
        }
        if (i == '0') {
            return 0;
        }
        int ways = process(chars, i + 1);
        if (i + 1 < chars.length && (chars[i] - '0') * 10 + chars[i + 1] - '0' < 27) {
            ways += process(chars, i + 2);
        }
        return ways;
    }

    public static int getWaysByDp(String str) {
        if (str == null || str.length() == 0) {
            return 0;
        }
        char[] chars = str.toCharArray();
        int[] dp = new int[chars.length + 1];
        dp[chars.length] = 1;
        for (int i = chars.length - 1; i >= 0; i--) {
            if (chars[i] == '0') {
                continue;
            }
            int ways = dp[i + 1];
            if (i + 1 < chars.length && (chars[i] - '0') * 10 + chars[i + 1] - '0' < 27) {
                ways += dp[i + 2];
            }
            dp[i] = ways;
        }
        return dp[0];
    }
}
