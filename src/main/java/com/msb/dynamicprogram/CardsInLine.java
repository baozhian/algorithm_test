package com.msb.dynamicprogram;

/**
 * @author bza625858
 * @version : CardsInLine.java, v 0.1 2024-07-25 23:03 bza625858 Exp $$
 */
public class CardsInLine {

    public static void main(String[] args) {
        int[] arr = {1, 10, 3, 4, 5, 6};
        // 6 4 10
        // 5 3 1
        System.out.println("1:" + winPlayCards(arr));
        System.out.println("2:" + winPlayCardsByCache(arr));
        System.out.println("3:" + winPlayCardsByDynamic(arr));
    }

    public static int winPlayCards(int[] arr) {
        int f = f(arr, 0, arr.length - 1);
        int g = g(arr, 0, arr.length - 1);
        System.out.println(f);
        System.out.println(g);
        return Math.max(f, g);
    }

    private static int f(int[] arr, int left, int right) {
        if (left == right) {
            return arr[left];
        }
        int leftMax = arr[left] + g(arr, left + 1, right);
        int rightMax = arr[right] + g(arr, left, right - 1);
        return Math.max(leftMax, rightMax);
    }

    private static int g(int[] arr, int left, int right) {
        if (left == right) {
            return 0;
        }
        int leftMax = f(arr, left + 1, right);
        int rightMax = f(arr, left, right - 1);
        return Math.min(leftMax, rightMax);
    }


    public static int winPlayCardsByCache(int[] arr) {
        int length = arr.length;
        int[][] fmap = new int[length][length];
        int[][] gmap = new int[length][length];
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                fmap[i][j] = -1;
                gmap[i][j] = -1;
            }
        }
        int f = fByCache(arr, 0, length - 1, fmap, gmap);
        int g = gByCache(arr, 0, length - 1, fmap, gmap);
        System.out.println(f);
        System.out.println(g);
        return Math.max(f, g);
    }

    private static int fByCache(int[] arr, int left, int right, int[][] fmap, int[][] gmap) {
        if (fmap[left][right] != -1) {
            return fmap[left][right];
        }
        int result;
        if (left == right) {
            result = arr[left];
        } else {
            int leftMax = arr[left] + gByCache(arr, left + 1, right, fmap, gmap);
            int rightMax = arr[right] + gByCache(arr, left, right - 1, fmap, gmap);
            result = Math.max(leftMax, rightMax);
        }
        fmap[left][right] = result;
        return result;
    }

    private static int gByCache(int[] arr, int left, int right, int[][] fmap, int[][] gmap) {
        if (gmap[left][right] != -1) {
            return gmap[left][right];
        }
        int result = 0;
        if (left != right) {
            int leftMax = fByCache(arr, left + 1, right, fmap, gmap);
            int rightMax = fByCache(arr, left, right - 1, fmap, gmap);
            result = Math.min(leftMax, rightMax);
        }
        gmap[left][right] = result;
        return result;
    }

    public static int winPlayCardsByDynamic(int[] arr) {
        int length = arr.length;
        int[][] fmap = new int[length][length];
        int[][] gmap = new int[length][length];
        for (int i = 0; i < length; i++) {
            fmap[i][i] = arr[i];
            gmap[i][i] = 0;
        }
        for (int col = 1; col < length; col++) {
            int r = 0;
            int c = col;
            while (c < length) {
                gmap[r][c] = Math.min(fmap[r][c - 1], fmap[r + 1][c]);
                fmap[r][c] = Math.max(arr[c] + gmap[r][c - 1], arr[r] + gmap[r + 1][c]);
                r++;
                c++;
            }
        }
        return Math.max(fmap[0][length - 1], gmap[0][length - 1]);
    }
}
