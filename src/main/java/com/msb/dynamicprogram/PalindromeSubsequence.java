package com.msb.dynamicprogram;

/**
 * @author bza625858
 * @version : PalindromeSubsequence.java, v 0.1 2024-08-03 13:53 bza625858 Exp $$
 */
public class PalindromeSubsequence {

    public static void main(String[] args) {
        System.out.println(maxLength("bba"));
        System.out.println(maxLength2("bba"));
        System.out.println(longestPalindromeSubseq("bba"));
    }

    public static int maxLength(String str) {
        StringBuilder builder = new StringBuilder(str);
        StringBuilder reverse = builder.reverse();
        return LongestCommonSubsequence.longestCommonSubsequence2(str, reverse.toString());
    }

    public static int maxLength2(String str) {
        char[] chars = str.toCharArray();
        return process(chars, 0, chars.length - 1);
    }

    private static int process(char[] chars, int l, int r) {
        if (l == r) {
            return 1;
        }
        if (l == r - 1) {
            return chars[l] == chars[r] ? 2 : 1;
        }
        int p1 = process(chars, l + 1, r - 1);
        int p2 = process(chars, l, r - 1);
        int p3 = process(chars, l + 1, r);
        int p4 = chars[l] == chars[r] ? (2 + p1) : 0;
        return Math.max(p1, Math.max(p2, Math.max(p3, p4)));
    }

    public static int longestPalindromeSubseq(String s) {
        char[] chars = s.toCharArray();
        int[][] ways = new int[chars.length][chars.length];
        ways[ways.length - 1][ways.length - 1] = 1;
        for (int i = 0; i < ways.length - 1; i++) {
            ways[i][i] = 1;
            ways[i][i + 1] = chars[i] == chars[i + 1] ? 2 : 1;
        }
        for (int col = 2; col < chars.length; col++) {
            int c = col;
            int row = 0;
            while (c < chars.length) {
                int p2 = ways[row][c - 1];
                int p3 = ways[row + 1][c];
                int p4 = chars[row] == chars[c] ? (2 + ways[row + 1][c - 1]) : 0;
                ways[row][c] = Math.max(p2, Math.max(p3, p4));
                c++;
                row++;
            }
        }
        return ways[0][chars.length - 1];
    }
}


