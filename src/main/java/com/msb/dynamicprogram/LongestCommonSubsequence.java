package com.msb.dynamicprogram;

/**
 * @author bza625858
 * @version : LongestCommonSubsequence.java, v 0.1 2024-08-03 12:36 bza625858 Exp $$
 */
public class LongestCommonSubsequence {

    public static void main(String[] args) {
        System.out.println(longestCommonSubsequence("ab123cf", "123ef"));
        System.out.println(longestCommonSubsequence2("ab123cf", "123ef"));
    }

    public static int longestCommonSubsequence(String str1, String str2) {
        return process(str1.toCharArray(), str2.toCharArray(), str1.length() - 1, str2.length() - 1);
    }

    private static int process(char[] chars1, char[] chars2, int i, int j) {
        if (i == 0 && j == 0) {
            return chars1[i] == chars2[j] ? 1 : 0;
        }
        if (i == 0) {
            if (chars1[i] == chars2[j]) {
                return 1;
            } else {
                return process(chars1, chars2, i, j - 1);
            }
        }
        if (j == 0) {
            if (chars1[i] == chars2[j]) {
                return 1;
            } else {
                return process(chars1, chars2, i - 1, j);
            }
        }
        int p1 = process(chars1, chars2, i, j - 1);
        int p2 = process(chars1, chars2, i - 1, j);
        int p3 = chars1[i] == chars2[j] ? 1 + process(chars1, chars2, i - 1, j - 1) : 0;
        return Math.max(p1, Math.max(p2, p3));
    }

    public static int longestCommonSubsequence2(String text1, String text2) {
        char[] chars1 = text1.toCharArray();
        char[] chars2 = text2.toCharArray();

        int[][] dp = new int[chars1.length][chars2.length];
        dp[0][0] = chars1[0] == chars2[0] ? 1 : 0;
        for (int col = 1; col < chars2.length; col++) {
            dp[0][col] = chars1[0] == chars2[col] ? 1 : dp[0][col - 1];
        }
        for (int row = 1; row < chars1.length; row++) {
            dp[row][0] = chars1[row] == chars2[0] ? 1 : dp[row - 1][0];
        }
        for (int i = 1; i < chars1.length; i++) {
            for (int j = 1; j < chars2.length; j++) {
                int p1 = dp[i][j - 1];
                int p2 = dp[i - 1][j];
                int p3 = chars1[i] == chars2[j] ? 1 + dp[i - 1][j - 1] : 0;
                dp[i][j] = Math.max(p1, Math.max(p2, p3));
            }
        }
        return dp[text1.length() - 1][text2.length() - 1];
    }
}
