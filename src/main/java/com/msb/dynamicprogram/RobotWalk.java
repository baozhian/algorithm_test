package com.msb.dynamicprogram;

/**
 * @author bza625858
 * @version : RobotWalk.java, v 0.1 2024-07-22 21:25 bza625858 Exp $$
 */
public class RobotWalk {

    public static void main(String[] args) {
        System.out.println(getWays(5, 2, 4, 6));
        System.out.println(getWays_v2(5, 2, 4, 6));
        System.out.println(getWays_v3(5, 2, 4, 6));
    }

    public static int getWays(int length, int curIdx, int aimIdx, int step) {
        // 1 ~ max
        if (step == 0) {
            return curIdx == aimIdx ? 1 : 0;
        }
        if (curIdx == 1) {
            return getWays(length, curIdx + 1, aimIdx, step - 1);
        }
        if (curIdx == length) {
            return getWays(length, curIdx - 1, aimIdx, step - 1);
        }
        return getWays(length, curIdx - 1, aimIdx, step - 1)
                + getWays(length, curIdx + 1, aimIdx, step - 1);
    }

    public static int getWays_v2(int length, int curIdx, int aimIdx, int step) {
        // 1 ~ max
        int[][] dp = new int[length + 1][step + 1];
        for (int i = 0; i <= length; i++) {
            for (int j = 0; j <= step; j++) {
                dp[i][j] = -1;
            }
        }
        return process(length, curIdx, aimIdx, step, dp);
    }

    private static int process(int length, int curIdx, int aimIdx, int step, int[][] dp) {
        if (dp[curIdx][step] != -1) {
            return dp[curIdx][step];
        }
        int ans;
        if (step == 0) {
            ans = curIdx == aimIdx ? 1 : 0;
        } else if (curIdx == 1) {
            ans = process(length, curIdx + 1, aimIdx, step - 1, dp);
        } else if (curIdx == length) {
            ans = process(length, curIdx - 1, aimIdx, step - 1, dp);
        } else {
            ans = process(length, curIdx - 1, aimIdx, step - 1, dp)
                    + process(length, curIdx + 1, aimIdx, step - 1, dp);
        }
        dp[curIdx][step] = ans;
        return ans;
    }

    public static int getWays_v3(int length, int curIdx, int aimIdx, int step) {
        // 1 ~ max
        int[][] dp = new int[length + 1][step + 1];
        dp[aimIdx][0] = 1;
        for (int i = 1; i <= step; i++) {  // 当前剩余步数 列
            // 左下
            dp[1][i] = dp[2][i - 1];
            for (int j = 2; j < length; j++) { // 当前行数 行
                // 左上 + 左下
                dp[j][i] = dp[j - 1][i - 1] + dp[j + 1][i - 1];
            }
            // 左上
            dp[length][i] = dp[length - 1][i - 1];  // 左上
        }
        return dp[curIdx][step];
    }
}
