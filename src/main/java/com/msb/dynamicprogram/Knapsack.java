package com.msb.dynamicprogram;

/**
 * @author bza625858
 * @version : Knapsack.java, v 0.1 2024-07-27 23:28 bza625858 Exp $$
 */
public class Knapsack {

    public static void main(String[] args) {
        int[] w = {3, 2, 4, 7};
        int[] v = {5, 6, 3, 19};
        System.out.println("1:" + maxValue(w, v, 11));
        System.out.println("2:" + maxValueByCache(w, v, 11));
        System.out.println("3:" + maxValueByDynamic(w, v, 11));
    }

    public static int maxValue(int[] w, int[] v, int bag) {
        return process(w, v, bag, 0);
    }

    private static int process(int[] w, int[] v, int bag, int idx) {
        if (bag < 0) {
            return -1;
        }
        if (idx >= w.length) {
            return 0;
        }
        int yes = process(w, v, bag - w[idx], idx + 1);
        if (yes != -1) {
            yes = v[idx] + yes;
        }
        int no = process(w, v, bag, idx + 1);
        return Math.max(yes, no);
    }

    public static int maxValueByCache(int[] w, int[] v, int bag) {
        int[][] dp = new int[w.length][bag + 1];
        for (int i = 0; i < dp.length; i++) {
            for (int j = 0; j < dp[0].length; j++) {
                dp[i][j] = -1;
            }
        }
        return process(w, v, bag, 0, dp);
    }

    private static int process(int[] w, int[] v, int rest, int idx, int[][] dp) {
        if (rest < 0) {
            return -1;
        }
        if (dp[idx][rest] != -1) {
            return dp[idx][rest];
        }
        int ans;
        if (idx < w.length) {
            int yes = process(w, v, rest - w[idx], idx + 1);
            if (yes != -1) {
                yes = v[idx] + yes;
            }
            int no = process(w, v, rest, idx + 1);
            ans = Math.max(yes, no);
        } else {
            ans = 0;
        }
        dp[idx][rest] = ans;
        return ans;
    }

    public static int maxValueByDynamic(int[] w, int[] v, int bag) {
        int[][] dp = new int[w.length + 1][bag + 1];
        int N = dp.length - 1;
        //  dp[N][...] = 0;
        for (int idx = N - 1; idx >= 0; idx--) {
            for (int rest = 0; rest < dp[0].length; rest++) {
                int no = dp[idx + 1][rest];
                int yes = rest - w[idx] < 0 ? -1 : dp[idx + 1][rest - w[idx]];
                if (yes != -1) {
                    yes = v[idx] + yes;
                }
                dp[idx][rest] = Math.max(yes, no);
            }
        }
        return dp[0][bag];
    }
}
