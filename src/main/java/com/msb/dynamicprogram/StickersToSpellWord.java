package com.msb.dynamicprogram;

import java.util.HashMap;
import java.util.Map;

/**
 * @author bza625858
 * @version : StickersToSpellWord.java, v 0.1 2024-07-31 21:45 bza625858 Exp $$
 */
public class StickersToSpellWord {

    public static void main(String[] args) {
        int result = minStickers1(new String[]{"ab", "bc", "abcd", "e"}, "ababcef");
        System.out.println(result);
        int result2 = minStickers2(new String[]{"ab", "bc", "abcd", "e"}, "ababcef");
        System.out.println(result2);

        int result3 = minStickers3(new String[]{"ab", "bc", "abcd", "e"}, "ababcef");
        System.out.println(result3);
    }

    public static int minStickers1(String[] stickers, String target) {
        int process = process(stickers, target);
        return process == Integer.MAX_VALUE ? -1 : process;
    }

    private static int process(String[] stickers, String target) {
        if (target.length() == 0) {
            return 0;
        }
        int ans = Integer.MAX_VALUE;
        for (String sticker : stickers) {
            String rest = minus(sticker, target);
            if (!rest.equals(target)) {
                ans = Math.min(ans, process(stickers, rest));
            }
        }
        return ans + (ans == Integer.MAX_VALUE ? 0 : 1);
    }

    private static String minus(String sticker, String rest) {
        int[] counts = new int[26];
        for (char c : rest.toCharArray()) {
            int idx = c - 'a';
            counts[idx]++;
        }
        for (char c : sticker.toCharArray()) {
            int idx = c - 'a';
            counts[idx]--;
        }
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 26; i++) {
            if (counts[i] > 0) {
                for (int j = 0; j < counts[i]; j++) {
                    builder.append((char) (i + 'a'));
                }
            }
        }
        return builder.toString();
    }

    public static int minStickers2(String[] stickers, String target) {
        int[][] stickerArr = new int[stickers.length][26];
        for (int i = 0; i < stickers.length; i++) {
            String sticker = stickers[i];
            for (char c : sticker.toCharArray()) {
                stickerArr[i][c - 'a']++;
            }
        }
        int process = process(stickerArr, target);
        return process == Integer.MAX_VALUE ? -1 : process;
    }

    private static int process(int[][] stickerArr, String target) {
        if (target.length() == 0) {
            return 0;
        }
        char[] targetChars = target.toCharArray();
        int[] targetArr = new int[26];
        for (char c : targetChars) {
            targetArr[c - 'a']++;
        }
        int min = Integer.MAX_VALUE;
        for (int[] sticker : stickerArr) {
            if (sticker[targetChars[0] - 'a'] > 0) {
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < targetArr.length; i++) {
                    if (targetArr[i] > 0) {
                        int nums = targetArr[i] - sticker[i];
                        for (int k = 0; k < nums; k++) {
                            builder.append((char) (i + 'a'));
                        }
                    }
                }
                String rest = builder.toString();
                min = Math.min(min, process(stickerArr, rest));
            }
        }
        return min + (min == Integer.MAX_VALUE ? 0 : 1);
    }

    public static int minStickers3(String[] stickers, String target) {
        int[][] stickerArr = new int[stickers.length][26];
        for (int i = 0; i < stickers.length; i++) {
            String sticker = stickers[i];
            for (char c : sticker.toCharArray()) {
                stickerArr[i][c - 'a']++;
            }
        }
        Map<String, Integer> cache = new HashMap<>();
        cache.put("", 0);
        int process = process(stickerArr, target, cache);
        return process == Integer.MAX_VALUE ? -1 : process;
    }

    private static int process(int[][] stickerArr, String target, Map<String, Integer> cache) {
        if (cache.containsKey(target)) {
            return cache.get(target);
        }
        char[] targetChars = target.toCharArray();
        int[] targetArr = new int[26];
        for (char c : targetChars) {
            targetArr[c - 'a']++;
        }
        int min = Integer.MAX_VALUE;
        for (int[] sticker : stickerArr) {
            if (sticker[targetChars[0] - 'a'] > 0) {
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < targetArr.length; i++) {
                    if (targetArr[i] > 0) {
                        int nums = targetArr[i] - sticker[i];
                        for (int k = 0; k < nums; k++) {
                            builder.append((char) (i + 'a'));
                        }
                    }
                }
                String rest = builder.toString();
                min = Math.min(min, process(stickerArr, rest));
            }
        }
        return min + (min == Integer.MAX_VALUE ? 0 : 1);
    }
}
