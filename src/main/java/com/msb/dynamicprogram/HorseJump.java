package com.msb.dynamicprogram;

/**
 * @author bza625858
 * @version : HorseJump.java, v 0.1 2024-08-03 21:15 bza625858 Exp $$
 */
public class HorseJump {

    /**
     * 从(0,0)出发走target步，到达(a,b)的方法数
     * 棋盘 9 * 10
     */
    public static int jump(int a, int b, int target) {
        return process(a, b, 0, 0, target);
    }

    private static int process(int a, int b, int x, int y, int rest) {
        if (x < 0 || y < 0 || x > 8 || y > 9) {
            return 0;
        }
        if (rest == 0) {
            return a == x && b == y ? 1 : 0;
        }
        int ways = process(a, b, x - 1, y - 2, rest - 1);
        ways += process(a, b, x - 1, y + 2, rest - 1);

        ways += process(a, b, x + 1, y - 2, rest - 1);
        ways += process(a, b, x + 1, y + 2, rest - 1);

        ways += process(a, b, x - 2, y + 1, rest - 1);
        ways += process(a, b, x - 2, y - 1, rest - 1);

        ways += process(a, b, x + 2, y + 1, rest - 1);
        ways += process(a, b, x + 2, y - 1, rest - 1);
        return ways;
    }

    public static int jumpByDp(int a, int b, int target) {
        int[][][] dp = new int[9][10][target];
        dp[a][b][0] = 1;

        for (int rest = 1; rest < target; rest++) {
            for (int x = 0; x < dp.length; x++) {
                for (int y = 0; y < dp[0].length; y++) {
                    int ways = pick(dp, x - 1, y - 2, rest - 1);
                    ways += pick(dp, x - 1, y + 2, rest - 1);

                    ways += pick(dp, x + 1, y - 2, rest - 1);
                    ways += pick(dp, x + 1, y + 2, rest - 1);

                    ways += pick(dp, x - 2, y + 1, rest - 1);
                    ways += pick(dp, x - 2, y - 1, rest - 1);

                    ways += pick(dp, x + 2, y + 1, rest - 1);
                    ways += pick(dp, x + 2, y - 1, rest - 1);
                    dp[x][y][rest] = ways;
                }
            }
        }
        return dp[0][0][target];
    }

    private static int pick(int[][][] dp, int x, int y, int rest) {
        if (x < 0 || y < 0 || x > 8 || y > 9) {
            return 0;
        }
        return dp[x][y][rest];
    }
}