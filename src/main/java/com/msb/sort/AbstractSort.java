package com.msb.sort;

/**
 * Copyright (C), 2024-01-19
 *
 * @author bza
 * @version v1.0.0
 * @create AbstractSort.java 2024-01-19 22:16
 */
public abstract class AbstractSort<T extends Comparable<T>> implements SortAble<T> {

    protected void swap(int prev, int next, T[] data) {
        T temp = data[prev];
        data[prev] = data[next];
        data[next] = temp;
    }

}
