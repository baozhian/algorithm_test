package com.msb.sort;

/**
 * Copyright (C), 2024-01-19
 *
 * @author bza
 * @version v1.0.0
 * @create SortAble.java 2024-01-19 22:16
 */
public interface SortAble<T extends Comparable<T>> {

    void sort(T[] data);
}
