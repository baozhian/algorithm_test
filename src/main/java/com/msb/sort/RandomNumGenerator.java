package com.msb.sort;

import java.util.*;

/**
 * Copyright (C), 2024-01-19
 *
 * @author bza
 * @version v1.0.0
 * @create RandomNumGenerator.java 2024-01-19 22:16
 */
public class RandomNumGenerator {

    private static final Random RAND = new Random();

    public static Integer[] generateNum(int min, int max, int count) {
        Set<Integer> numbers = new HashSet<>(count);
        while (numbers.size() < count) {
            int number = RAND.nextInt((max - min) + 1) + min;
            numbers.add(number);
        }
        return numbers.toArray(new Integer[0]);
    }

    public static void main(String[] args) {
        int min = 1; // 最小值
        int max = 500; // 最大值
        int count = 50; // N个随机数

        Integer[] randomNumbers = generateNum(min, max, count);
        System.out.println("生成的" + count + "个随机数为：" + Arrays.toString(randomNumbers));
    }
}