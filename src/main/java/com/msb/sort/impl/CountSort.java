package com.msb.sort.impl;

import com.msb.sort.AbstractSort;

/**
 * @author bza625858
 * @version : CountSort.java, v 0.1 2024-06-22 14:36 bza625858 Exp $$
 */
public class CountSort extends AbstractSort<Integer> {

    @Override
    public void sort(Integer[] data) {
        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;
        for (Integer ele : data) {
            max = Math.max(max, ele);
            min = Math.min(min, ele);
        }
        int[] helper = new int[max - min + 1];
        for (int i = 0; i < data.length; i++) {
            helper[data[i] - min]++;
        }
        int idx = 0;
        for (int i = 0; i < helper.length; i++) {
            while (helper[i]-- > 0) {
                data[idx++] = i + min;
            }
        }
    }
}
