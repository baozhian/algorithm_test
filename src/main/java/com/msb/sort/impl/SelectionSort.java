package com.msb.sort.impl;

import com.msb.sort.AbstractSort;

/**
 * Copyright (C), 2024-01-19
 *
 * @author bza
 * @version v1.0.0
 * @create SelectionSort.java 2024-01-19 22:16
 */
public class SelectionSort<T extends Comparable<T>> extends AbstractSort<T> {

    @Override
    public void sort(T[] data) {
        // 0 ~ n-1
        // 1 ~ n-1
        // 2 ~ n-1
        // ...
        for (int i = 0; i < data.length - 1; i++) {
            // 最小值在哪个位置 i ~ n-1
            int minIdx = i;
            for (int j = i + 1; j < data.length; j++) {
                minIdx = data[minIdx].compareTo(data[j]) > 0 ? j : minIdx;
            }
            swap(minIdx, i, data);
        }
    }
}
