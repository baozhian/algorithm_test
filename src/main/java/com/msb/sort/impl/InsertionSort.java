package com.msb.sort.impl;

import com.msb.sort.AbstractSort;

/**
 * Copyright (C), 2024-02-05
 *
 * @author bza
 * @version v1.0.0
 * @create InsertionSort.java 2024-02-05 21:55
 */
public class InsertionSort<T extends Comparable<T>> extends AbstractSort<T> {

    @Override
    public void sort(T[] data) {
        for (int i = 1; i < data.length; i++) {
            for (int j = i; j > 0; j--) {
                if (data[j].compareTo(data[j - 1]) < 0) {
                    swap(j, j - 1, data);
                }
            }
        }
    }
}
