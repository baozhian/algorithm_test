package com.msb.sort.impl;

/**
 * Copyright (C), 2024-06-11
 *
 * @author bza
 * @version v1.0.0
 * @create NonRecursiveMergeSort.java 2024-06-11 20:25
 */
public class NonRecursiveMergeSort<T extends Comparable<T>> extends MergeSort<T> {

    @Override
    public void sort(T[] data) {
        int length = data.length;
        int mergeSize = 1;
        while (mergeSize < length) {
            int left = 0;
            while (left < length) {
                int middle = left + mergeSize - 1;
                if (middle > length) {
                    break;
                }
                int end = Math.min(middle + mergeSize, length - 1);
                merge(data, left, middle, end);
                left = end + 1;
            }
            if (mergeSize > length / 2) {
                break;
            }
            mergeSize <<= 1;
        }

    }
}
