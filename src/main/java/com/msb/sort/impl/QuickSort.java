package com.msb.sort.impl;

import com.msb.sort.AbstractSort;

/**
 * Copyright (C), 2024-06-03
 *
 * @author bza
 * @version v1.0.0
 * @create QuickSort.java 2024-06-03 20:26
 */
public class QuickSort<T extends Comparable<T>> extends AbstractSort<T> {

    @Override
    public void sort(T[] data) {
        sort(data, 0, data.length - 1);
    }

    protected void sort(T[] data, int start, int end) {
        if (start > end) {
            return;
        }
        int zoneIdx = partition(data, start, end);
        if (zoneIdx > start) {
            sort(data, start, zoneIdx - 1);
        }
        if (zoneIdx < end) {
            sort(data, zoneIdx + 1, end);
        }
    }

    private int partition(T[] data, int start, int end) {
        if (start == end) {
            return start;
        }
        int pivot = (int) (start + Math.random() * (end - start + 1));
        swap(pivot, end, data);
        int zoneIdx = start - 1;
        for (int i = start; i <= end; i++) {
            if (data[i].compareTo(data[end]) <= 0) {
                zoneIdx++;
                if (zoneIdx < i) {
                    swap(zoneIdx, i, data);
                }
            }
        }
        return zoneIdx;
    }
}
