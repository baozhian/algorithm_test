package com.msb.sort.impl;

import com.msb.sort.AbstractSort;

/**
 * Copyright (C), 2024-06-03
 *
 * @author bza
 * @version v1.0.0
 * @create ShellSort.java 2024-06-03 21:04
 */
public class ShellSort<T extends Comparable<T>> extends AbstractSort<T> {

    @Override
    public void sort(T[] data) {
        int gap = 0;
        while (gap < data.length / 3) {
            gap = gap * 3 + 1;
        }
        while (gap > 0) {
            for (int i = gap; i < data.length; i++) {
                for (int j = i - gap; j >= 0; j -= gap) {
                    if (data[j].compareTo(data[j + gap]) > 0) {
                        swap(j, j + gap, data);
                    }
                }

            }
            gap = gap / 2;
        }
    }
}
