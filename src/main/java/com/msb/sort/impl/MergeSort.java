package com.msb.sort.impl;

import com.msb.sort.AbstractSort;

import java.lang.reflect.Array;

/**
 * Copyright (C), 2024-06-03
 *
 * @author bza
 * @version v1.0.0
 * @create MergeSort.java 2024-06-03 21:42
 */
public class MergeSort<T extends Comparable<T>> extends AbstractSort<T> {

    @Override
    public void sort(T[] data) {
        sort(data, 0, data.length - 1);
    }

    private void sort(T[] data, int start, int end) {
        if (start >= end) {
            return;
        }
        int middle = start + (end - start) >> 1;
        sort(data, start, middle);
        sort(data, middle + 1, end);
        merge(data, start, middle, end);
    }

    protected void merge(T[] data, int start, int middle, int end) {
        int leftPtr = start;
        int rightPtr = middle + 1;
        @SuppressWarnings("unchecked")
        T[] helper = (T[]) Array.newInstance(data[0].getClass(), end - start + 1);
        int i = 0;
        while (leftPtr <= middle && rightPtr <= end) {
            helper[i++] = data[leftPtr].compareTo(data[rightPtr]) <= 0 ? data[leftPtr++] : data[rightPtr++];
        }
        while (leftPtr <= middle) {
            helper[i++] = data[leftPtr++];
        }
        while (rightPtr <= end) {
            helper[i++] = data[rightPtr++];
        }
        for (int j = 0; j < helper.length; j++) {
            data[start + j] = helper[j];
        }
    }
}
