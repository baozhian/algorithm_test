package com.msb.sort.impl;

import com.msb.sort.AbstractSort;

/**
 * Copyright (C), 2024-01-19
 *
 * @author bza
 * @version v1.0.0
 * @create BubbleSort.java 2024-01-19 23:31
 */
public class BubbleSort<T extends Comparable<T>> extends AbstractSort<T> {

    @Override
    public void sort(T[] data) {
        // 0 ~ n-1
        // 0 ~ n-2
        // 0 ~ n-3
        // ...
        for (int i = data.length - 1; i > 0; i--) {
            // 0 ~ i 比较交换
            for (int j = 0; j < i; j++) {
                if (data[j].compareTo(data[j + 1]) > 0) {
                    swap(j, j + 1, data);
                }
            }
        }
    }
}
