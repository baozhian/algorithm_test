package com.msb.sort.impl;

/**
 * Copyright (C), 2024-06-12
 *
 * @author bza
 * @version v1.0.0
 * @create QuickSortV2.java 2024-06-12 20:40
 */
public class QuickSortV2<T extends Comparable<T>> extends QuickSort<T> {


    protected void sort(T[] data, int start, int end) {
        if (start > end) {
            return;
        }
        int[] zoneIdx = partition(data, start, end);
        sort(data, start, zoneIdx[0] - 1);
        sort(data, zoneIdx[1] + 1, end);
    }

    private int[] partition(T[] data, int start, int end) {
        if (start > end) {
            return new int[]{-1, -1};
        }
        if (start == end) {
            return new int[]{start, start};
        }
        int pivot = (int) (start + Math.random() * (end - start + 1));
        swap(pivot, end, data);
        int less = start - 1;
        int more = end;
        int idx = start;
        while (idx < more) {
            if (data[idx].compareTo(data[end]) < 0) {
                swap(idx++, ++less, data);
            } else if (data[idx].compareTo(data[end]) == 0) {
                idx++;
            } else {
                swap(idx, --more, data);
            }
        }
        swap(more, end, data);
        return new int[]{less + 1, more};
    }
}
