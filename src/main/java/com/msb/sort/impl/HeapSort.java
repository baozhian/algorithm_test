package com.msb.sort.impl;

import com.msb.sort.AbstractSort;

/**
 * Copyright (C), 2024-06-15
 *
 * @author bza
 * @version v1.0.0
 * @create HeapSort.java 2024-06-15 11:57
 */
public class HeapSort<T extends Comparable<T>> extends AbstractSort<T> {

    @Override
    public void sort(T[] data) {
        if (data == null || data.length < 2) {
            return;
        }
        for (int i = 0; i < data.length; i++) {
            siftUp(data, i);
        }
        int heapSize = data.length;
        while (heapSize > 0) {
            swap(0, --heapSize, data);
            siftDown(data, 0, heapSize);
        }
    }

    private void siftUp(T[] data, int childIdx) {
        while (data[childIdx].compareTo(data[(childIdx - 1) / 2]) > 0) {
            swap(childIdx, (childIdx - 1) / 2, data);
            childIdx = (childIdx - 1) / 2;
        }
    }

    private void siftDown(T[] data, int parentIdx, int heapSize) {
        int childIdx;
        while ((childIdx = parentIdx * 2 + 1) < heapSize) {
            if (childIdx + 1 < heapSize) {
                childIdx = data[childIdx + 1].compareTo(data[childIdx]) > 0 ? childIdx + 1 : childIdx;
            }
            childIdx = data[childIdx].compareTo(data[parentIdx]) > 0 ? childIdx : parentIdx;
            if (childIdx == parentIdx) {
                break;
            }
            swap(childIdx, parentIdx, data);
            parentIdx = childIdx;
        }
    }


}
