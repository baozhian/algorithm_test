package com.msb.sort.impl;

import com.msb.sort.AbstractSort;

/**
 * @author bza625858
 * @version : RadixSort.java, v 0.1 2024-06-22 16:35 bza625858 Exp $$
 */
public class RadixSort extends AbstractSort<Integer> {


    @Override
    public void sort(Integer[] data) {
        int maxEle = 0;
        for (Integer integer : data) {
            maxEle = Math.max(maxEle, integer);
        }
        int maxBit = 0;
        while (maxEle != 0) {
            maxBit++;
            maxEle /= 10;
        }
        sort(0, data.length - 1, data, maxBit);
    }

    private static int getDigit(int num, int d) {
        return (num / (int) Math.pow(10, d - 1)) % 10;
    }


    private void sort(int start, int end, Integer[] data, int maxBit) {
        int[] helper = new int[end - start + 1];
        for (int i = 1; i <= maxBit; i++) {
            int[] count = new int[10];
            for (int j = start; j <= end; j++) {
                int digit = getDigit(data[j], i);
                count[digit]++;
            }
            for (int k = 1; k < count.length; k++) {
                count[k] = count[k - 1] + count[k];
            }
            for (int n = end; n >= start; n--) {
                int digit = getDigit(data[n], i);
                helper[--count[digit]] = data[n];
            }
            for (int m = 0; m < helper.length; m++) {
                data[start + m] = helper[m];
            }
        }
    }
}
