package com.msb.link;

import com.msb.model.DualListNode;
import com.msb.model.ListNode;

/**
 * Copyright (C), 2024-06-08
 *
 * @author bza
 * @version v1.0.0
 * @create DeleteGiveNum.java 2024-06-08 14:55
 */
public class DeleteGiveNum {

    public static void main(String[] args) {
        ListNode head = LinkUtils.getListNode(8);

        System.out.println(head.print());
        System.out.println(head.next.next.val);
        System.out.println(deleteGiveNum(head, head.next.next.val).print());
    }

    public static ListNode deleteGiveNum(ListNode head, int num) {
        while (head != null) {
            if (head.val != num) {
                break;
            }
            head = head.next;
        }
        ListNode cur = head;
        ListNode pre = head;
        while (cur != null) {
            if (cur.val == num) {
                pre.next = cur.next;
            } else {
                pre = cur;
            }
            cur = cur.next;
        }
        return head;
    }
}
