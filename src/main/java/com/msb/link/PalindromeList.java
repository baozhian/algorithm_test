package com.msb.link;

import com.msb.model.ListNode;

import java.util.Stack;

/**
 * @author bza625858
 * @version : PalindromeList.java, v 0.1 2024-06-23 14:08 bza625858 Exp $$
 */
public class PalindromeList {

    public static boolean isPalindromeByStack(ListNode head) {
        if (head == null || head.next == null) {
            return false;
        }
        Stack<Integer> stack = new Stack<>();
        ListNode curPtr = head;
        while (curPtr != null) {
            stack.push(curPtr.val);
            curPtr = curPtr.next;
        }
        while (head != null) {
            int pop = stack.pop();
            if (head.val != pop) {
                return false;
            }
            head = head.next;
        }
        return true;
    }

    public static boolean isPalindrome(ListNode head) {
        if (head == null) {
            return false;
        }
        if (head.next == null) {
            return true;
        }
        ListNode fast = head;
        ListNode slow = head;
        while (fast.next != null && fast.next.next != null) {
            fast = fast.next.next;
            slow = slow.next;
        }
        ListNode downMid = slow.next;
        if (downMid == null) {
            return false;
        }
        ListNode start = head;
        ListNode middle = reverse(downMid);
        while (middle != null) {
            if (start.val != middle.val) {
                return false;
            }
            middle = middle.next;
            start = start.next;
        }
        return true;
    }

    private static ListNode reverse(ListNode cur) {
        ListNode pre = null;
        while (cur != null) {
            ListNode next = cur.next;
            cur.next = pre;
            pre = cur;
            cur = next;
        }
        return pre;
    }

    public static void main(String[] args) {
        ListNode node = LinkUtils.getPalindromeList(9);
        System.out.println(node.print());
        System.out.println(isPalindrome(node));

        node = LinkUtils.getPalindromeList(14);
        System.out.println(node.print());
        System.out.println(isPalindrome(node));
    }
}
