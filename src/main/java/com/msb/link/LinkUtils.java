package com.msb.link;

import com.msb.model.DualListNode;
import com.msb.model.ListNode;
import com.msb.model.RNode;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

/**
 * Copyright (C), 2024-06-08
 *
 * @author bza
 * @version v1.0.0
 * @create LinkUtils.java 2024-06-08 14:23
 */
public class LinkUtils {

    private static final Supplier<Integer> supplier = () -> (int) (Math.random() * 90) + 10;

    public static ListNode getListNode(int len) {
        ListNode head = null;
        ListNode pre = null;
        for (int i = 0; i < len; i++) {
            ListNode node = new ListNode(supplier.get());
            if (head == null) {
                head = node;
            }
            if (pre != null) {
                pre.next = node;
            }
            pre = node;
        }
        return head;
    }

    public static DualListNode getDualListNode(int len) {
        DualListNode head = null;
        DualListNode pre = null;
        for (int i = 0; i < len; i++) {
            DualListNode node = new DualListNode(supplier.get());
            if (head == null) {
                head = node;
            }
            node.prev = pre;
            if (pre != null) {
                pre.next = node;
            }
            pre = node;
        }
        return head;
    }

    public static ListNode getPalindromeList(int len) {
        ListNode head = null;
        ListNode pre = null;
        List<Integer> objects = new ArrayList<>(len / 2);
        for (int i = 0; i < len / 2; i++) {
            int i1 = supplier.get();
            objects.add(i1);
            ListNode node = new ListNode(i1);
            if (head == null) {
                head = node;
            }
            if (pre != null) {
                pre.next = node;
            }
            pre = node;
        }
        if (len % 2 != 0) {
            ListNode node = new ListNode(supplier.get());
            pre.next = node;
            pre = node;
        }
        for (int i = objects.size() - 1; i >= 0; i--) {
            ListNode node = new ListNode(objects.get(i));
            pre.next = node;
            pre = node;
        }
        return head;
    }

    public static RNode getRandList(int len) {
        RNode head = null;
        RNode pre = null;
        List<RNode> objects = new ArrayList<>();
        for (int i = 0; i < len; i++) {
            RNode node = new RNode(supplier.get());
            objects.add(node);
            if (head == null) {
                head = node;
            }
            if (pre != null) {
                pre.next = node;
            }
            pre = node;
        }
        for (int i = 0; i < objects.size(); i++) {
            if (i != objects.size() - 1) {
                objects.get(i).rand = objects.get(i + 1);
            }
        }
        return head;
    }
}
