package com.msb.link;

import com.msb.model.Node;

import java.util.AbstractQueue;
import java.util.Iterator;
import java.util.Queue;

/**
 * Copyright (C), 2024-06-08
 *
 * @author bza
 * @version v1.0.0
 * @create LinkQueue.java 2024-06-08 16:30
 */
public class LinkQueue<E> extends AbstractQueue<E> {

    private Node<E> head;

    private Node<E> tail;

    private int size;

    public LinkQueue() {
        this.head = null;
        this.tail = null;
    }

    public static void main(String[] args) {
        Queue<Integer> queue = new LinkQueue<>();
        queue.offer(1);
        queue.offer(2);
        System.out.println("添加1、2：" + queue);
        queue.offer(3);
        queue.offer(4);
        System.out.println("添加3、4：" + queue);

        System.out.println("取出一个(" + queue.poll() + ")：" + queue);
        queue.offer(5);
        queue.offer(6);
        System.out.println("添加5、6：" + queue);
        System.out.println("取出一个(" + queue.poll() + ")：" + queue);
        System.out.println("取出一个(" + queue.poll() + ")：" + queue);
        System.out.println("取出一个(" + queue.poll() + ")：" + queue);
        System.out.println("取出一个(" + queue.poll() + ")：" + queue);
        System.out.println("取出一个(" + queue.poll() + ")：" + queue);
        queue.offer(3);
        queue.offer(4);
        System.out.println("添加3、4：" + queue);
    }

    @Override
    public Iterator<E> iterator() {
        return null;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean offer(E e) {
        Node<E> node = new Node<>(e);
        if (tail == null) {
            head = node;
        } else {
            tail.next = node;
        }
        tail = node;
        size++;
        return true;
    }

    @Override
    public E poll() {
        Node<E> node = this.head;
        if (node == null) {
            return null;
        }
        this.head = node.next;
        node.next = null;
        if (this.head == null) {
            this.tail = null;
        }
        size--;
        return node.val;
    }

    @Override
    public E peek() {
        return this.head == null ? null : this.head.val;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        Node<E> head = this.head;
        while (head != null) {
            builder.append(head.val).append(" -> ");
            head = head.next;
        }
        return builder.toString();
    }
}
