package com.msb.link;

import com.msb.model.RNode;

/**
 * @author bza625858
 * @version : LinkedListCopy.java, v 0.1 2024-06-23 18:44 bza625858 Exp $$
 */
public class LinkedListCopy {

    public static RNode copyLinkedList(RNode head) {
        if (head == null) {
            return null;
        }
        RNode cur = head;
        while (cur != null) {
            RNode next = cur.next;
            cur.next = new RNode(cur.val);
            cur.next.next = next;
            cur = next;
        }
        cur = head;
        while (cur != null) {
            RNode curCopy = cur.next;
            RNode next = curCopy.next;
            curCopy.rand = curCopy.rand != null ? curCopy.rand.next : null;
            cur = next;
        }
        cur = head;
        RNode resNode = head.next;
        while (cur != null && cur.next != null) {
            RNode curCopy = cur.next;
            RNode next = cur.next.next;
            cur.next = next;

            curCopy.next = next != null ? next.next : null;
            cur = next;
        }
        return resNode;
    }

    public static void main(String[] args) {
        RNode head = LinkUtils.getRandList(15);
        RNode head2 = LinkUtils.getRandList(14);

        System.out.println(head.print());
        RNode node = copyLinkedList(head);
        System.out.println(node.print());

        System.out.println(head2.print());
        RNode node2 = copyLinkedList(head2);
        System.out.println(node2.print());

    }
}
