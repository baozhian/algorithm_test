package com.msb.link;

import com.msb.model.ListNode;

/**
 * Copyright (C), 2024-06-08
 *
 * @author bza
 * @version v1.0.0
 * @create ReverseSingleLink.java 2024-06-08 13:59
 */
public class ReverseSingleLink {

    public static void main(String[] args) {
        ListNode head = LinkUtils.getListNode(5);

        System.out.println(head.print());
        ListNode node = reverseLink(head);
        System.out.println(node.print());
    }

    public static ListNode reverseLink(ListNode head) {
        ListNode preNode = null;
        while (head != null) {
            ListNode next = head.next;
            head.next = preNode;
            preNode = head;
            head = next;
        }
        return preNode;
    }
}
