package com.msb.link;

import com.msb.model.ListNode;

/**
 * @author bza625858
 * @version : LinkedListHasIntersect.java, v 0.1 2024-06-24 21:43 bza625858 Exp $$
 */
public class LinkedListHasIntersect {

    /**
     * 判断两个链表是否相交，如果相交，求交点，没有相交返回null
     *
     * @param head1 链表1
     * @param head2 链表2
     * @return 两个链表相交的交点
     */
    public static ListNode getIntersectionNode(ListNode head1, ListNode head2) {
        // 两个链表相交无环
        // 两个链表相交有环
        // 不存在一个无环一个有环
        ListNode loop1 = getLoopNode(head1);
        ListNode loop2 = getLoopNode(head2);
        if (loop1 == null && loop2 == null) {
            return notExistLoop(head1, head2);
        }
        if (loop1 != null && loop2 != null) {
            return bothExistLoop(head1, head2, loop1, loop2);
        }
        return null;
    }

    private static ListNode getLoopNode(ListNode head) {
        if (head == null || head.next == null || head.next.next == null) {
            return null;
        }
        ListNode fast = head.next;
        ListNode slow = head.next.next;
        while (fast != slow) {
            if (fast.next == null || fast.next.next == null) {
                return null;
            }
            fast = fast.next.next;
            slow = slow.next;
        }
        fast = head;
        while (fast != slow) {
            if (fast.next == null || slow.next == null) {
                return null;
            }
            fast = fast.next;
            slow = slow.next;
        }
        return slow;
    }

    private static ListNode notExistLoop(ListNode head1, ListNode head2) {
        if (head1 == null || head2 == null) {
            return null;
        }
        int length = 0;
        ListNode start1 = head1;
        while (start1.next != null) {
            length++;
            start1 = start1.next;
        }
        ListNode start2 = head2;
        while (start2.next != null) {
            length--;
            start2 = start2.next;
        }
        if (start1 != start2) {
            return null;
        }
        start1 = length > 0 ? head1 : head2;
        start2 = start1 == head1 ? head2 : head1;
        length = Math.abs(length);
        while (length-- > 0) {
            start1 = start1.next;
        }
        while (start1 != start2) {
            start1 = start1.next;
            start2 = start2.next;
        }
        return start1;
    }

    private static ListNode bothExistLoop(ListNode head1, ListNode head2,
                                          ListNode loop1, ListNode loop2) {
        if (loop1 == loop2) {
            // 有同一个环，头结点不在环内
            int length = 0;
            ListNode start1 = head1;
            while (start1 != loop1) {
                length++;
                start1 = start1.next;
            }
            ListNode start2 = head2;
            while (start2 != loop2) {
                length--;
                start2 = start2.next;
            }
            start1 = length > 0 ? head1 : head2;
            start2 = start1 == head1 ? head2 : head1;
            length = Math.abs(length);
            while (length-- > 0) {
                start1 = start1.next;
            }
            while (start1 != start2) {
                start1 = start1.next;
                start2 = start2.next;
                if (start1 == null || start2 == null) {
                    return null;
                }
            }
            return start1;
        }
        // 有同一个环，其中一个头结点在环内
        ListNode start1 = loop1.next;
        while (start1 != head1) {
            if (start1 == loop2) {
                return start1;
            }
            start1 = start1.next;
        }
        return null;
    }
}
