package com.msb.link;

import com.msb.model.ListNode;

/**
 * @author bza625858
 * @version : LinkedListPartition.java, v 0.1 2024-06-23 16:14 bza625858 Exp $$
 */
public class LinkedListPartition {

    public static ListNode partition(ListNode head, int pivot) {
        ListNode smallHead = null;
        ListNode smallTair = null;

        ListNode midlHead = null;
        ListNode midlTair = null;

        ListNode bigHead = null;
        ListNode bigTair = null;
        while (head != null) {
            ListNode next = head.next;
            head.next = null;
            int val = head.val;
            if (val < pivot) {
                if (smallHead == null) {
                    smallHead = head;
                } else {
                    smallTair.next = head;
                }
                smallTair = head;
            } else if (val == pivot) {
                if (midlHead == null) {
                    midlHead = head;
                } else {
                    midlTair.next = head;
                }
                midlTair = head;
            } else {
                if (bigHead == null) {
                    bigHead = head;
                } else {
                    bigTair.next = head;
                }
                bigTair = head;
            }
            head = next;
        }
        if (smallTair != null) {
            smallTair.next = midlHead;
            midlTair = midlTair == null ? smallTair : midlTair;
        }
        if (midlTair != null) {
            midlTair.next = bigHead;
        }
        if (smallHead != null) {
            return smallHead;
        }
        if (midlHead != null) {
            return midlHead;
        }
        return bigHead;
    }

    public static void main(String[] args) {
        int pivot = 56;
        ListNode node = LinkUtils.getListNode(15);
        System.out.println(node.print(pivot));
        ListNode listNode = LinkedListPartition.partition(node, pivot);
        System.out.println(listNode.print(pivot));
    }
}
