package com.msb.link;

import com.msb.model.DualListNode;
import com.msb.model.ListNode;

/**
 * Copyright (C), 2024-06-08
 *
 * @author bza
 * @version v1.0.0
 * @create ReverseDualLink.java 2024-06-08 14:09
 */
public class ReverseDualLink {

    public static void main(String[] args) {
        DualListNode head = LinkUtils.getDualListNode(4);

        System.out.println(head.print());
        ListNode node = reverseLink(head);
        System.out.println("==========================");
        System.out.println(node.print());
    }

    public static ListNode reverseLink(DualListNode head) {
        DualListNode preNode = null;
        while (head != null) {
            DualListNode next = (DualListNode) head.next;
            head.next = preNode;
            head.prev = next;

            preNode = head;
            head = next;
        }
        return preNode;
    }
}
