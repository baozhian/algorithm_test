package com.msb.link;

import com.msb.model.ListNode;

/**
 * @author bza625858
 * @version : LinkedListHasCirCle.java, v 0.1 2024-06-23 20:00 bza625858 Exp $$
 */
public class LinkedListHasCirCle {

    /**
     * 判断一个链表是否有环
     *
     * @param head 链表头
     * @return 有环，返回交点，没有返回null
     */
    public static ListNode hasCirCle(ListNode head) {
        if (head == null || head.next == null || head.next.next == null) {
            return null;
        }
        ListNode fast = head.next;
        ListNode slow = head.next.next;
        while (fast != slow) {
            if (fast.next == null || fast.next.next == null) {
                return null;
            }
            fast = fast.next.next;
            slow = slow.next;
        }
        fast = head;
        while (fast != slow) {
            fast = fast.next;
            slow = slow.next;
        }
        return slow;
    }
}
