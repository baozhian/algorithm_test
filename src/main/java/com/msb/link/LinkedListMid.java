package com.msb.link;

import com.msb.model.ListNode;

/**
 * @author bza625858
 * @version : LinkedListMid.java, v 0.1 2024-06-22 22:29 bza625858 Exp $$
 */
public class LinkedListMid {

    // 1）输入链表头节点，奇数长度返回中点，偶数长度返回上中点
    public static ListNode getMidOrUpMid(ListNode head) {
        if (head == null) {
            return null;
        }
        ListNode slowPtr = head;
        ListNode fastPtr = head;
        while (fastPtr.next != null && fastPtr.next.next != null) {
            fastPtr = fastPtr.next.next;
            slowPtr = slowPtr.next;
        }
        return slowPtr;
    }

    // 2)输入链表头节点，奇数长度返回中点，偶数长度返回下中点
    public static ListNode getMidOrDownMid(ListNode head) {
        ListNode slowPtr = head;
        ListNode fastPtr = head;
        while (fastPtr != null && fastPtr.next != null) {
            fastPtr = fastPtr.next.next;
            slowPtr = slowPtr.next;
        }
        return slowPtr;
    }

    // 3）输入链表头节点，奇数长度返回中点前一个，偶数长度返回上中点前一个
    public static ListNode getMidPreOrUpMidPre(ListNode head) {
        if (head == null || head.next == null || head.next.next == null) {
            return null;
        }
        ListNode slowPtr = head;
        ListNode fastPtr = head.next.next;
        while (fastPtr.next != null && fastPtr.next.next != null) {
            fastPtr = fastPtr.next.next;
            slowPtr = slowPtr.next;
        }
        return slowPtr;
    }

    // 4）输入链表头节点，奇数长度返回中点前一个，偶数长度返回下中点前一个
    public static ListNode getMidPreOrDownMid(ListNode head) {
        if (head == null || head.next == null) {
            return null;
        }
        ListNode slowPtr = head;
        ListNode fastPtr = head.next.next;
        while (fastPtr != null && fastPtr.next != null) {
            fastPtr = fastPtr.next.next;
            slowPtr = slowPtr.next;
        }
        return slowPtr;
    }

    public static void main(String[] args) {
        ListNode head = LinkUtils.getListNode(15);
        ListNode head2 = LinkUtils.getListNode(14);

        System.out.println(head.print());
        ListNode node = getMidPreOrUpMidPre(head);
        System.out.println(node.val);

        System.out.println(head2.print());
        ListNode node2 = getMidPreOrUpMidPre(head2);
        System.out.println(node2.val);

    }
}
