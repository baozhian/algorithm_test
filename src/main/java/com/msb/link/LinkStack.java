package com.msb.link;

import com.msb.model.DualNode;
import com.msb.model.Node;

import java.util.AbstractQueue;
import java.util.Iterator;
import java.util.Queue;
import java.util.Stack;

/**
 * Copyright (C), 2024-06-08
 *
 * @author bza
 * @version v1.0.0
 * @create LinkStack.java 2024-06-08 16:30
 */
public class LinkStack<E> extends Stack<E> {

    private DualNode<E> head;

    private DualNode<E> tail;

    private int size;

    public LinkStack() {
        this.head = null;
        this.tail = null;
    }

    public static void main(String[] args) {
        LinkStack<Integer> stack = new LinkStack<>();
        stack.push(1);
        stack.push(2);
        System.out.println("添加1、2：" + stack);
        stack.push(3);
        stack.push(4);
        System.out.println("添加3、4：" + stack);

        System.out.println("取出一个(" + stack.pop() + ")：" + stack);
        stack.push(5);
        stack.push(6);
        System.out.println("添加5、6：" + stack);
        System.out.println("取出一个(" + stack.pop() + ")：" + stack);
        System.out.println("取出一个(" + stack.pop() + ")：" + stack);
        System.out.println("取出一个(" + stack.pop() + ")：" + stack);
        System.out.println("取出一个(" + stack.pop() + ")：" + stack);
        System.out.println("取出一个(" + stack.pop() + ")：" + stack);
        stack.push(3);
        stack.push(4);
        System.out.println("添加3、4：" + stack);
    }


    @Override
    public E push(E item) {
        DualNode<E> node = new DualNode<>(item);
        DualNode<E> tail = this.tail;
        if (tail == null) {
            this.head = node;
        } else {
            tail.next = node;
            node.prev = tail;
        }
        this.tail = node;
        return item;
    }

    @Override
    public synchronized E pop() {
        DualNode<E> node = this.tail;
        if (node == null) {
            this.head = null;
            return null;
        }
        DualNode<E> prev = node.prev;
        this.tail = prev;
        if (prev != null) {
            prev.next = null;
        }
        node.prev = null;
        node.next = null;
        return node.val;
    }

    @Override
    public synchronized E peek() {
        return this.tail == null ? null : this.tail.val;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        DualNode<E> head = this.head;
        while (head != null) {
            builder.append(head.val).append(" -> ");
            head = (DualNode<E>) head.next;
        }
        return builder.toString();
    }
}
