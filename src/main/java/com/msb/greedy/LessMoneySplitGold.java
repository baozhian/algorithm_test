package com.msb.greedy;

import java.util.PriorityQueue;

/**
 * @author bza625858
 * @version : LessMoneySplitGold.java, v 0.1 2024-07-11 21:37 bza625858 Exp $$
 */
public class LessMoneySplitGold {

    public static int getLessMoney(int[] arr) {
        PriorityQueue<Integer> queue = new PriorityQueue<>();
        for (int i : arr) {
            queue.add(i);
        }
        int ans = 0;
        while (queue.size() >= 2) {
            int sum = queue.poll() + queue.poll();
            ans += sum;
            queue.add(sum);
        }
        return ans;
    }
}
