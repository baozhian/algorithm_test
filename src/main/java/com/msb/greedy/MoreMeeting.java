package com.msb.greedy;

import java.util.Arrays;
import java.util.Comparator;

/**
 * @author bza625858
 * @version : MoreMeeting.java, v 0.1 2024-07-10 22:21 bza625858 Exp $$
 */
public class MoreMeeting {

    private static class Meeting {

        int start;

        int end;

        public Meeting(int start, int end) {
            this.start = start;
            this.end = end;
        }

        public int getEnd() {
            return end;
        }
    }

    public static int getMoreMeeting(Meeting[] meetings) {
        Arrays.sort(meetings, Comparator.comparingInt(Meeting::getEnd));
        int timeLine = 0;
        int ans = 0;
        for (Meeting meeting : meetings) {
            if (meeting.start >= timeLine) {
                ans++;
                timeLine = meeting.end;
            }
        }
        return ans;
    }

    public static int getMoreMeeting_(Meeting[] meetings) {
        return process(meetings, 0, 0);
    }

    private static int process(Meeting[] meetings, int done, int timeLine) {
        if (meetings.length == 0) {
            return done;
        }
        int ans = done;
        for (int i = 0; i < meetings.length; i++) {
            if (meetings[i].start >= timeLine) {
                Meeting[] meetingArr = getMeetings(meetings, i);
                ans = Math.max(ans, process(meetingArr, done + 1, meetings[i].end));
            }
        }
        return ans;
    }

    private static Meeting[] getMeetings(Meeting[] meetings, int idx) {
        int num = 0;
        Meeting[] result = new Meeting[meetings.length - 1];
        for (int i = 0; i < meetings.length; i++) {
            if (i == idx) {
                continue;
            }
            result[num++] = meetings[i];
        }
        return result;
    }

    public static void main(String[] args) {
        Meeting[] result = new Meeting[10];
        int num = 0;
        result[num++] = new Meeting(1, 3);
        result[num++] = new Meeting(2, 4);
        result[num++] = new Meeting(3, 5);
        result[num++] = new Meeting(3, 7);
        result[num++] = new Meeting(4, 6);
        result[num++] = new Meeting(7, 9);
        result[num++] = new Meeting(8, 12);
        result[num++] = new Meeting(10, 15);
        result[num++] = new Meeting(12, 18);
        result[num] = new Meeting(15, 20);
        System.out.println(getMoreMeeting(result));
        System.out.println(getMoreMeeting_(result));
    }
}
