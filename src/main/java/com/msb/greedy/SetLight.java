package com.msb.greedy;

import java.util.Objects;

/**
 * Copyright (C), 2024-07-13
 *
 * @author bza
 * @version v1.0.0
 * @create SetLight.java 2024-07-13 14:14
 */
public class SetLight {

    public static int getMinLight(String line) {
        if (Objects.isNull(line)) {
            return 0;
        }
        char[] chars = line.toCharArray();
        int idx = 0;
        int light = 0;
        while (idx < chars.length) {
            if (chars[idx] == 'X') {
                idx++;
                continue;
            }
            light++;
            if (idx + 1 == chars.length) {
                // 后面就要结束了，i位置必须放一个灯
                break;
            } else if (chars[idx + 1] == 'X') {
                // 下一个位置是一堵墙，i位置放灯
                idx = idx + 2;
            } else {
                // 其他情况，i+1位置放灯
                idx = idx + 3;
            }
        }
        return light;
    }
}
