package com.msb.model;

/**
 * @author bza625858
 * @version : RNode.java, v 0.1 2024-06-23 18:43 bza625858 Exp $$
 */
public class RNode {

    public RNode rand;
    public int val;
    public RNode next;

    public RNode(int val) {
        this.val = val;
    }

    public String print() {
        StringBuilder builder = new StringBuilder();

        RNode head = this;
        while (head != null) {
            builder.append(head.val).append(" -> ");
            head = head.next;
        }
        return builder.toString();
    }
}
