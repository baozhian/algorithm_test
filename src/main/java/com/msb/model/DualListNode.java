package com.msb.model;

/**
 * Copyright (C), 2024-06-08
 *
 * @author bza
 * @version v1.0.0
 * @create DualListNode.java 2024-06-08 14:08
 */
public class DualListNode extends ListNode {

    public DualListNode prev;

    public DualListNode(int val) {
        super(val);
    }

    public String print() {
        StringBuilder builder = new StringBuilder();
        DualListNode head = this;
        DualListNode last = null;
        while (head != null) {
            builder.append(head.val).append(" -> ");
            head = (DualListNode) head.next;
            if (head != null) {
                last = head;
            }
        }
        builder.append(" |  ");
        while (last != null) {
            builder.append(last.val).append(" -> ");
            last = last.prev;
        }
        return builder.toString();
    }
}
