package com.msb.model;

public class DualNode<E> extends Node<E> {
    public E val;
    public DualNode<E> prev;

    public DualNode() {
    }

    public DualNode(E val) {
        this.val = val;
    }

}