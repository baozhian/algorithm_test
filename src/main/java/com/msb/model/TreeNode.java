package com.msb.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class TreeNode {
    public Integer val;
    public TreeNode left;
    public TreeNode right;

    public TreeNode() {
    }

    public TreeNode(Integer val) {
        this.val = val;
    }

    TreeNode(Integer val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }


    // 左右节点分支, 分隔字符, 叶子节点基础间隔
    private static final String TREE_LEFT_BRANCH = "/";
    private static final String TREE_RIGHT_BRANCH = "\\";
    private static final String TREE_SPLIT = "  ";
    private static final int TREE_BASE_INTERVAL = 3;

    public static TreeNode arrayToBTree(Integer [] arr) {
        // 判空处理，返回空节点
        if (arr == null || arr.length == 0) {
            return new TreeNode();
        }
        // 创建和数组长度相同的集合
        List<TreeNode> nodes = new ArrayList<>(arr.length);
        // 遍历数组，将数组元素转为集合节点
        for (Integer obj : arr) {
            TreeNode treeNode = new TreeNode();
            treeNode.val = obj;
            nodes.add(treeNode);
        }
        //  按照完全二叉树的规则构建，数组中的后半部分元素都是叶子节点，它们没有左右子节点。所以循环只需要处理前半部分的非叶子节点即可。
        //  i < arrs.length/2 - 1  能够将所有左右子节点不为null的元素给遍历出来，剩下最后一个（在左节点上）或者最后两个（在右节点上）
        //  保证循环只在前半部分有效的节点范围内进行迭代，避免处理不必要的叶子节点。
        for (int i = 0; i < arr.length / 2 - 1; i++) {
            TreeNode node = nodes.get(i);
            // 首先，由于是通过数组构建的二叉树（数组下标从0开始）
            // 其次，由完全二叉树的性质（从1开始计数，左孩子为2i，右孩子为2i  +1 ）  再结合 数组下标0 开始计数 可知：
            // 树的左节点为 2i +1；树的右节点为：2i +2
            node.left = nodes.get(i * 2 + 1);
            node.right = nodes.get(i * 2 + 2);
        }
        // 只有当总节点数是奇数时，最后一个父节点才有右子节点
        int lastPNodeIndex = arr.length / 2 - 1;
        TreeNode lastPNode = nodes.get(lastPNodeIndex);
        // 左孩子节点
        lastPNode.left = nodes.get(lastPNodeIndex * 2 + 1);
        if (arr.length % 2 != 0) {
            lastPNode.right = nodes.get(lastPNodeIndex * 2 + 2);
        }

        return nodes.get(0);
    }

    /**
     * 二叉树打印;
     *
     * @param head 树根节点
     */
    public static void treePrint(TreeNode head) {
        List<List<TreeNode>> allNodes = new ArrayList<>();
        List<TreeNode> curLvNodes = Collections.singletonList(head);
        allNodes.add(curLvNodes);
        while (true) {
            List<TreeNode> list = new ArrayList<>();
            for (TreeNode node : curLvNodes) {
                if (node == null) {
                    list.add(null);
                    list.add(null);
                } else {
                    list.add(node.left);
                    list.add(node.right);
                }
            }
            if (list.stream().allMatch(Objects::isNull)) {
                break;
            }
            curLvNodes = list;
            allNodes.add(curLvNodes);
        }
        StringBuilder sb = new StringBuilder();
        int allLevel = allNodes.size();
        for (int i = 0; i < allNodes.size(); i++) {
            List<TreeNode> nodes = allNodes.get(i);
            Object[] lvValue = new Object[nodes.size()];
            for (int i1 = 0; i1 < nodes.size(); i1++) {
                TreeNode node = nodes.get(i1);
                if (node == null) {
                    lvValue[i1] = TREE_SPLIT;
                } else {
                    lvValue[i1] = "" + node.val;
                }
            }
            String levelString = getLevelString(lvValue, i, allLevel);
            sb.append(levelString).append("\n");
        }
        System.out.println("tree print:\n" + sb);
    }

    /**
     * 返回该层打印字符串;
     *
     * @param levelArray 该层的数组
     * @param level      该层层号
     * @param totalLevel 总高度
     * @return 该层打印字符串
     */
    private static String getLevelString(Object[] levelArray, int level, int totalLevel) {
        int levelInterval = (int) Math.pow(2, totalLevel - level + 1) - 1;
        int leftBlank = (levelInterval - TREE_BASE_INTERVAL) / 2;
        String levelIntervalStr = TREE_SPLIT.repeat(levelInterval);

        StringBuilder sb = new StringBuilder();
        if (level > 0) {
            if (leftBlank > 0) {
                sb.append(TREE_SPLIT.repeat(leftBlank));
            }
            for (int i = 0; i < levelArray.length; i++) {
                if (TREE_SPLIT.equals(levelArray[i])) {
                    sb.append(TREE_SPLIT);
                } else {
                    if (i % 2 == 0) {
                        sb.append(TREE_LEFT_BRANCH);
                    } else {
                        sb.append(TREE_RIGHT_BRANCH);
                    }
                }
                if (i < levelArray.length - 1) {
                    sb.append(levelIntervalStr);
                }
            }
            sb.append("\n");
        }
        sb.append(TREE_SPLIT.repeat(leftBlank));
        for (int i = levelArray.length; i > 0; i--) {
            sb.append(levelArray[levelArray.length - i].toString());
            if (i > 1) {
                sb.append(levelIntervalStr);
            }
        }
        return sb.toString();
    }


}