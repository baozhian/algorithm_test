package com.msb.model;

import com.msb.sort.RandomNumGenerator;
import lombok.Data;

/**
 * Copyright (C), 2024-06-15
 *
 * @author bza
 * @version v1.0.0
 * @create LineSegment.java 2024-06-15 16:15
 */
@Data
public class LineSegment {

    private int start;

    private int end;

    public LineSegment() {
    }

    public LineSegment(int start, int end) {
        this.start = start;
        this.end = end;
    }

    public static LineSegment[] getRandom(int size) {
        Integer[] start = RandomNumGenerator.generateNum(1, 25, size / 2);
        Integer[] end = RandomNumGenerator.generateNum(25, 50, size / 2);
        LineSegment[] segments = new LineSegment[size / 2];
        for (int i = 0; i < segments.length; i++) {
            LineSegment segment = new LineSegment();
            segment.setStart(start[i]);
            segment.setEnd(end[i]);
            segments[i] = segment;
        }
        return segments;
    }

    @Override
    public String toString() {
        return start + " -> " + end;
    }
}
