package com.msb.model;

public class ListNode {
    public int val;
    public ListNode next;

    public ListNode() {
    }

    public ListNode(int val) {
        this.val = val;
    }

    public ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }


    public String print() {
        StringBuilder builder = new StringBuilder();

        ListNode head = this;
        while (head != null) {
            builder.append(head.val).append(" -> ");
            head = head.next;
        }
        return builder.toString();
    }

    public String print(int num) {
        StringBuilder builder = new StringBuilder();
        ListNode head = this;
        while (head != null) {
            if (head.val == num) {
                builder.append("[").append(head.val).append("]");
            } else {
                builder.append(head.val);
            }
            builder.append(" -> ");
            head = head.next;
        }
        return builder.toString();
    }
}