package com.msb.model;

public class Node<T> {
    public T val;
    public Node<T> next;

    public Node() {
    }

    public Node(T val) {
        this.val = val;
    }

    public Node(T val, Node next) {
        this.val = val;
        this.next = next;
    }
}