package com.msb.recursion;

import java.util.ArrayList;
import java.util.List;

/**
 * @author bza625858
 * @version : PrintStringAllSubsequence.java, v 0.1 2024-07-18 22:30 bza625858 Exp $$
 */
public class PrintStringAllSubsequence {

    public static void main(String[] args) {
        printAllSubsequence("abc1");
        printAllArrange("abc1");
        printAllArrange_v2("abc1");
    }

    public static void printAllSubsequence(String chats) {
        char[] chars = chats.toCharArray();
        List<String> ans = new ArrayList<>();
        process(chars, 0, ans, "");
        System.out.println(ans);
    }

    private static void process(char[] chats, int idx, List<String> ans, String path) {
        if (idx == chats.length) {
            ans.add(path);
            return;
        }
        process(chats, idx + 1, ans, path);
        process(chats, idx + 1, ans, path + chats[idx]);
    }

    public static void printAllArrange(String chatStr) {
        char[] charArr = chatStr.toCharArray();
        List<Character> chats = new ArrayList<>();
        for (char c : charArr) {
            chats.add(c);
        }
        List<String> ans = new ArrayList<>();
        handle(chats, ans, "");
        System.out.println(ans);
    }

    private static void handle(List<Character> chats, List<String> ans, String path) {
        if (chats.isEmpty()) {
            ans.add(path);
            return;
        }
        for (int i = 0; i < chats.size(); i++) {
            Character character = chats.get(i);
            chats.remove(character);
            handle(chats, ans, path + character);
            chats.add(i, character);
        }
    }

    public static void printAllArrange_v2(String chatStr) {
        char[] charArr = chatStr.toCharArray();
        List<String> ans = new ArrayList<>();
        deal(charArr, 0, ans);
        System.out.println(ans);
    }

    private static void deal(char[] charArr, int index, List<String> ans) {
        if (index == charArr.length) {
            ans.add(String.valueOf(charArr));
            return;
        }
        boolean[] visited = new boolean[256];
        for (int i = index; i < charArr.length; i++) {
            if (visited[charArr[i]]) {
                continue;
            }
            visited[charArr[i]] = true;
            swap(charArr, i, index);
            deal(charArr, index + 1, ans);
            swap(charArr, i, index);
        }
    }

    private static void swap(char[] charArr, int i, int index) {
        if (i == index) {
            return;
        }
        char temp = charArr[i];
        charArr[i] = charArr[index];
        charArr[index] = temp;
    }
}
