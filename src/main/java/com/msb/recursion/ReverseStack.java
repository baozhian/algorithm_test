package com.msb.recursion;

import java.util.Arrays;
import java.util.Stack;

/**
 * @author bza625858
 * @version : ReverseStack.java, v 0.1 2024-07-20 16:16 bza625858 Exp $$
 */
public class ReverseStack {

    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<>();
        stack.push(1);
        stack.push(8);
        stack.push(3);
        stack.push(7);
        System.out.println(Arrays.toString(stack.toArray()));
        reverseStack(stack);
        System.out.println(Arrays.toString(stack.toArray()));
    }

    public static void reverseStack(Stack<Integer> stack) {
        if (stack.isEmpty()) {
            return;
        }
        int bottomEle = getBottomEle(stack);
        reverseStack(stack);
        stack.push(bottomEle);
    }

    private static int getBottomEle(Stack<Integer> stack) {
        Integer result = stack.pop();
        if (stack.isEmpty()) {
            // 如果result是栈底一个元素，直接返回
            return result;
        }
        // 如果result是倒数第二个元素
        int lastEle = getBottomEle(stack);
        stack.push(result);
        return lastEle;
    }
}
