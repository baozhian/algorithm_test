package com.msb.recursion;

/**
 * @author bza625858
 * @version : Hanoi.java, v 0.1 2024-07-18 22:19 bza625858 Exp $$
 */
public class Hanoi {

    /**
     * Recursively describes the process of moving n items from one place to another.
     * This function uses recursion to simulate the process of moving items, making it easier to understand the step-by-step process.
     * @param n The number of items to move, indicating the current batch of items to move.
     * @param from The current location of the items.
     * @param to The destination to move the items.
     * @param other A temporary location used during the moving process.
     */
    public static void func(int n, String from, String to, String other) {
        // When there is only one item, directly describe the movement action from the starting point to the destination
        if (n == 1) {
            System.out.println("move " + n + " from " + from + " to " + to);
        } else {
            // Move n-1 items from the original location to the temporary location, preparing to move the last item
            func(n - 1, from, other, to);
            // Move the last item from the original location to the destination
            System.out.println("move " + n + " from " + from + " to " + to);
            // Move the n-1 items that were temporarily placed to the final destination
            func(n - 1, other, to, from);
        }
    }


    public static void main(String[] args) {
        func(3, "A", "B", "C");
    }
}
