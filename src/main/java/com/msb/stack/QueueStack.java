package com.msb.stack;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/**
 * Copyright (C), 2024-06-09
 *
 * @author bza
 * @version v1.0.0
 * @create QueueStack.java 2024-06-09 13:06
 */
public class QueueStack<E> extends Stack<E> {

    private Queue<E> store;

    private Queue<E> helper;

    public QueueStack() {
        store = new LinkedList<>();
        helper = new LinkedList<>();
    }

    @Override
    public E push(E item) {
        store.add(item);
        return item;
    }

    @Override
    public synchronized E pop() {
        while (helper.size() > 1) {
            store.add(helper.poll());
        }
        E result = store.poll();
        Queue<E> store = this.store;
        this.store = this.helper;
        this.helper = store;
        return result;
    }

    @Override
    public synchronized E peek() {
        while (helper.size() > 1) {
            store.add(helper.poll());
        }
        E result = store.poll();
        helper.add(result);
        Queue<E> store = this.store;
        this.store = this.helper;
        this.helper = store;
        return result;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        Iterator<E> iterator = store.iterator();
        while (iterator.hasNext()) {
            builder.append(iterator.next()).append(" -> ");
        }
        return builder.toString();
    }


    public static void main(String[] args) {
        QueueStack<Integer> stack = new QueueStack<>();
        stack.push(1);
        stack.push(2);
        System.out.println("添加1、2：" + stack);
        stack.push(3);
        stack.push(4);
        System.out.println("添加3、4：" + stack);

        System.out.println("取出一个(" + stack.pop() + ")：" + stack);
        stack.push(5);
        stack.push(6);
        System.out.println("添加5、6：" + stack);
        System.out.println("取出一个(" + stack.pop() + ")：" + stack);
        System.out.println("取出一个(" + stack.pop() + ")：" + stack);
        System.out.println("取出一个(" + stack.pop() + ")：" + stack);
        System.out.println("取出一个(" + stack.pop() + ")：" + stack);
        System.out.println("取出一个(" + stack.pop() + ")：" + stack);
        stack.push(3);
        stack.push(4);
        System.out.println("添加3、4：" + stack);
    }
}
