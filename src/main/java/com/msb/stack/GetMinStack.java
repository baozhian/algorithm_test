package com.msb.stack;

import java.util.Iterator;
import java.util.Stack;

/**
 * Copyright (C), 2024-06-08
 *
 * @author bza
 * @version v1.0.0
 * @create GetMinStack.java 2024-06-08 21:27
 */
public class GetMinStack<E extends Comparable<E>> extends Stack<E> {

    private Stack<E> store;

    private Stack<E> minValue;

    public GetMinStack() {
        this.store = new Stack<>();
        this.minValue = new Stack<>();
    }

    public static void main(String[] args) {
        GetMinStack<Integer> stack = new GetMinStack<>();
        stack.push(1);
        stack.push(2);
        System.out.println("添加1、2：" + stack);
        System.out.println("最小值：" + stack.getMin());
        stack.push(3);
        stack.push(4);
        System.out.println("添加3、4：" + stack);
        System.out.println("最小值：" + stack.getMin());

        System.out.println("取出一个(" + stack.pop() + ")：" + stack);
        stack.push(5);
        stack.push(6);
        System.out.println("添加5、6：" + stack);
        System.out.println("取出一个(" + stack.pop() + ")：" + stack);
        System.out.println("最小值：" + stack.getMin());
        System.out.println("取出一个(" + stack.pop() + ")：" + stack);
        System.out.println("最小值：" + stack.getMin());
        System.out.println("取出一个(" + stack.pop() + ")：" + stack);
        System.out.println("最小值：" + stack.getMin());
        System.out.println("取出一个(" + stack.pop() + ")：" + stack);
        System.out.println("最小值：" + stack.getMin());
        System.out.println("取出一个(" + stack.pop() + ")：" + stack);
        System.out.println("最小值：" + stack.getMin());
        stack.push(3);
        stack.push(4);
        System.out.println("添加3、4：" + stack);
        System.out.println("最小值：" + stack.getMin());
    }


    @Override
    public E push(E item) {
        E oldMin;
        if (store.size() == 0) {
            minValue.push(item);
        } else if ((oldMin = minValue.peek()).compareTo(item) <= 0) {
            minValue.push(oldMin);
        } else if (oldMin.compareTo(item) > 0) {
            minValue.push(item);
        }
        store.push(item);
        return item;
    }

    @Override
    public synchronized E pop() {
        minValue.pop();
        return store.pop();
    }

    @Override
    public synchronized E peek() {
        return store.peek();
    }

    public E getMin(){
        if (minValue.size() == 0) {
            return null;
        }
        return minValue.peek();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        Iterator<E> iterator = store.iterator();
        while (iterator.hasNext()) {
            builder.append(iterator.next()).append(" -> ");
        }
        return builder.toString();
    }
}
