package com.msb.xor;

/**
 * Copyright (C), 2024-06-08
 *
 * @author bza
 * @version v1.0.0
 * @create FindOneOddNumInArr.java 2024-06-08 10:44
 */
public class FindOneOddNumInArr {

    public static void main(String[] args) {
        // —个数组中有一种数出现了奇数次，其他数都出现了偶数次
        // 怎么找到并打印这种数
        int[] data = {1, 1, 2, 3, 2, 4, 4, 5, 5, 7, 7, 5, 5, 3, 3};
        int result = 0;
        for (int datum : data) {
            result = result ^ datum;
        }
        System.out.println(result);
    }
}
