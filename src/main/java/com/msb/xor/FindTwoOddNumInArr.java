package com.msb.xor;

/**
 * Copyright (C), 2024-06-08
 *
 * @author bza
 * @version v1.0.0
 * @create FindTwoOddNumInArr.java 2024-06-08 11:24
 */
public class FindTwoOddNumInArr {

    public static void main(String[] args) {
        // —个数组中有两种数出现了奇数次，其他数都出现了偶数次
        // 怎么找到并打印这两种数
        int[] data = {1, 1, 2, 3, 2, 4, 4, 7, 7, 5, 5, 5};
        int eor = 0;
        for (int num : data) {
            eor ^= num;
        }
        int rightOne = FindFarRightOne.find(eor);
        int resultOne = 0;
        for (int num : data) {
            if ((num & rightOne) != 0) {
                resultOne ^= num;
            }
        }
        int resultTwo = eor ^ resultOne;
        System.out.println(resultOne);
        System.out.println(resultTwo);
    }
}
