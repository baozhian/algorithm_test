package com.msb.xor;

/**
 * Copyright (C), 2024-06-08
 *
 * @author bza
 * @version v1.0.0
 * @create ExchangeTwoNum.java 2024-06-08 10:41
 */
public class ExchangeTwoNum {

    public static void main(String[] args) {
        int[] data = {1, 4, 6};
        int i = 0;
        int j = 2;
        System.out.println(data[i] + "," + data[j]);
        data[i] = data[i] ^ data[j];
        data[j] = data[i] ^ data[j];
        data[i] = data[i] ^ data[j];
        System.out.println(data[i] + "," + data[j]);
    }
}
