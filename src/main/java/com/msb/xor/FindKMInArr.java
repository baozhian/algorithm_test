package com.msb.xor;

/**
 * Copyright (C), 2024-06-08
 *
 * @author bza
 * @version v1.0.0
 * @create FindKMInArr.java 2024-06-08 13:00
 */
public class FindKMInArr {

    public static void main(String[] args) {
        int[] dataArr = {-1, 1, 1, 1, 3, 3, 3, -1};
        // 请保证arr中，只有一种数出现了K次，其他数都出现了M次
        int k = 2, m = 3;
        System.out.println(onlyKTimes(dataArr, k, m));
    }

    public static int onlyKTimes(int[] arr, int k, int m) {
        int[] helper = new int[32];
        for (int num : arr) {
            for (int i = 0; i < 32; i++) {
                helper[i] += (num >> i) & 1;
            }
        }
        int result = 0;
        for (int i = 0; i < 32; i++) {
            if (helper[i] % m != 0) {
                result |= (1 << i);
            }
        }
        return result;
    }
}
