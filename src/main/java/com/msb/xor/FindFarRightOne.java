package com.msb.xor;

/**
 * Copyright (C), 2024-06-08
 *
 * @author bza
 * @version v1.0.0
 * @create FindFarRightOne.java 2024-06-08 10:51
 */
public class FindFarRightOne {

    public static void main(String[] args) {
        int a = 255554;
        printBinary(a);
        int result = a & (~a + 1);
        int result2 = a & (-a);
        printBinary(result);
        printBinary(result2);
    }

    public static int find(int a) {
        return a & (~a + 1);
    }

    private static void printBinary(int a) {
        for (int i = 31; i >= 0; i--) {
            if ((a & (1 << i)) != 0) {
                System.out.print("1");
            } else {
                System.out.print("0");
            }
        }
        System.out.println();
    }
}
