package com.msb.arr;

import java.util.AbstractQueue;
import java.util.Arrays;
import java.util.Iterator;

/**
 * Copyright (C), 2024-06-08
 *
 * @author bza
 * @version v1.0.0
 * @create ArrayQueue.java 2024-06-08 18:08
 */
public class ArrayQueue<E> extends AbstractQueue<E> {

    private int size;

    private int limit;

    private int offerIdx = 0;

    private int pollIdx = 0;

    private Object[] arr;

    public static void main(String[] args) {
        ArrayQueue<Integer> queue = new ArrayQueue<>(10);
        queue.offer(1);
        queue.offer(2);
        queue.offer(3);
        queue.offer(4);
        queue.offer(5);
        queue.offer(6);
        queue.offer(7);
        queue.offer(8);
        queue.offer(9);
        queue.offer(10);
        System.out.println(queue);
        System.out.println("取出一个(" + queue.poll() + ")：" + queue);
        queue.offer(10);
        System.out.println(queue);
        System.out.println("取出一个(" + queue.poll() + ")：" + queue);
        System.out.println("取出一个(" + queue.poll() + ")：" + queue);
        System.out.println("取出一个(" + queue.poll() + ")：" + queue);
        System.out.println("取出一个(" + queue.poll() + ")：" + queue);
        System.out.println("取出一个(" + queue.poll() + ")：" + queue);
        System.out.println("取出一个(" + queue.poll() + ")：" + queue);
        System.out.println("取出一个(" + queue.poll() + ")：" + queue);
        System.out.println("取出一个(" + queue.poll() + ")：" + queue);
        System.out.println("取出一个(" + queue.poll() + ")：" + queue);
        System.out.println("取出一个(" + queue.poll() + ")：" + queue);
    }

    public ArrayQueue(int limit) {
        this.limit = limit;
        arr = new Object[limit];
    }

    @Override
    public Iterator<E> iterator() {
        return null;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean offer(E e) {
        if (size() == limit) {
            throw new RuntimeException("队列中元素满了");
        }
        if (offerIdx == limit) {
            offerIdx = 0;
        }
        arr[offerIdx++] = e;
        size++;
        return true;
    }

    @Override
    public E poll() {
        if (size() == 0) {
            throw new RuntimeException("队列中已经没有元素了");
        }
        int pollIdx = this.pollIdx;
        if (pollIdx == limit) {
            pollIdx = this.pollIdx = 0;
        }
        Object o = arr[this.pollIdx++];
        arr[pollIdx] = null;
        size--;
        return (E) o;
    }

    @Override
    public E peek() {
        return (E) arr[this.pollIdx];
    }

    @Override
    public String toString() {
        return Arrays.toString(arr);
    }
}
