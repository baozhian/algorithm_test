package com.msb.arr;

import java.util.Arrays;
import java.util.Stack;

/**
 * Copyright (C), 2024-06-08
 *
 * @author bza
 * @version v1.0.0
 * @create ArrayStack.java 2024-06-08 18:08
 */
public class ArrayStack<E> extends Stack<E> {

    private Object[] arr;

    private int size;

    private int limit;

    private int pushIdx;

    public ArrayStack(int limit) {
        this.limit = limit;
        this.arr = new Object[limit];
    }

    public static void main(String[] args) {
        ArrayStack<Integer> stack = new ArrayStack<>(10);
        stack.push(1);
        stack.push(2);
        System.out.println("添加1、2：" + stack);
        stack.push(3);
        stack.push(4);
        System.out.println("添加3、4：" + stack);

        System.out.println("取出一个(" + stack.pop() + ")：" + stack);
        stack.push(5);
        stack.push(6);
        System.out.println("添加5、6：" + stack);
        System.out.println("取出一个(" + stack.pop() + ")：" + stack);
        System.out.println("取出一个(" + stack.pop() + ")：" + stack);
        System.out.println("取出一个(" + stack.pop() + ")：" + stack);
        System.out.println("取出一个(" + stack.pop() + ")：" + stack);
        System.out.println("取出一个(" + stack.pop() + ")：" + stack);
        stack.push(3);
        stack.push(4);
        System.out.println("添加3、4：" + stack);
    }

    @Override
    public synchronized int size() {
        return size;
    }

    @Override
    public E push(E item) {
        if (size() == limit) {
            throw new RuntimeException("栈中元素满了");
        }
        arr[pushIdx++] = item;
        size++;
        return item;
    }

    @Override
    public synchronized E pop() {
        if (size() == 0) {
            throw new RuntimeException("栈中已经没有元素了");
        }
        E e = (E) arr[pushIdx - 1];
        arr[pushIdx - 1] = null;
        pushIdx--;
        size--;
        return e;
    }

    @Override
    public synchronized E peek() {
        if (size() == 0) {
            throw new RuntimeException("栈中已经没有元素了");
        }
        return (E) arr[pushIdx - 1];
    }

    @Override
    public String toString() {
        return Arrays.toString(arr);
    }
}
