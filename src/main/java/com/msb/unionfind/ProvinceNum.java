package com.msb.unionfind;

/**
 * Copyright (C), 2024-07-14
 *
 * @author bza
 * @version v1.0.0
 * @create ProvinceNum.java 2024-07-14 13:36
 */
public class ProvinceNum {

    public int findCircleNum(int[][] isConnected) {
        UnionFind unionFind = new UnionFind(isConnected.length);
        for (int i = 0; i < isConnected.length; i++) {
            for (int j = i + 1; j < isConnected[0].length; j++) {
                if (isConnected[i][j] == 1) {
                    unionFind.merge(i, j);
                }
            }
        }
        return unionFind.sets();
    }


    private static class UnionFind {

        private int[] parent;

        private int[] size;

        private int sets;

        private int[] helper;

        public UnionFind(int size) {
            this.parent = new int[size];
            this.size = new int[size];
            this.sets = size;
            for (int i = 0; i < size; i++) {
                this.parent[i] = i;
                this.size[i] = 1;
            }
            this.helper = new int[size];
        }

        private int findTopParent(int cur) {
            int next;
            int idx = 0;
            while (cur != (next = this.parent[cur])) {
                this.helper[idx++] = cur;
                cur = next;
            }
            idx--;
            while (idx >= 0) {
                this.parent[this.helper[idx--]] = cur;
            }
            return cur;
        }


        public void merge(int a, int b) {
            int parent1 = findTopParent(a);
            int parent2 = findTopParent(b);
            if (parent1 == parent2) {
                return;
            }
            int size1 = this.size[parent1];
            int size2 = this.size[parent2];
            int big = size1 >= size2 ? parent1 : parent2;
            int small = parent1 == big ? parent2 : parent1;
            this.parent[small] = big;
            this.size[big] = size1 + size2;
            this.sets--;
        }


        public int sets() {
            return sets;
        }
    }


}
