package com.msb.unionfind;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

/**
 * Copyright (C), 2024-07-13
 *
 * @author bza
 * @version v1.0.0
 * @create UnionFind.java 2024-07-13 21:02
 */
public class UnionFind<V> {

    private static class Node<E> {
        private E val;

        public Node(E val) {
            this.val = val;
        }
    }

    private Map<V, Node<V>> nodeMap;

    private Map<Node<V>, Node<V>> parentMap;

    private Map<Node<V>, Integer> sizeMap;

    public UnionFind(List<V> data) {
        this.nodeMap = new HashMap<>();
        this.parentMap = new HashMap<>();
        this.sizeMap = new HashMap<>();

        for (V cur : data) {
            Node<V> node = new Node<>(cur);
            this.nodeMap.put(cur, node);
            this.parentMap.put(node, node);
            this.sizeMap.put(node, 1);
        }
    }


    public boolean isSameSet(V a, V b) {
        return findTopParent(a) == findTopParent(b);
    }

    private Node<V> findTopParent(V a) {
        Node<V> cur = this.nodeMap.get(a);
        if (cur == null) {
            return null;
        }
        Stack<Node<V>> stack = new Stack<>();
        Node<V> next;
        while (cur != (next = this.parentMap.get(cur))) {
            cur = next;
            stack.add(cur);
        }
        while (!stack.isEmpty()) {
            this.parentMap.put(stack.pop(), cur);
        }
        return cur;
    }


    public void merge(V a, V b) {
        Node<V> parent1 = findTopParent(a);
        Node<V> parent2 = findTopParent(b);
        if (parent1 == null || parent2 == null || parent1 == parent2) {
            return;
        }
        Integer size1 = this.sizeMap.get(parent1);
        Integer size2 = this.sizeMap.get(parent2);
        if (size1 == null || size2 == null) {
            return;
        }
        Node<V> big = size1 > size2 ? parent1 : parent2;
        Node<V> small = parent1 == big ? parent2 : parent1;
        this.parentMap.put(small, big);
        this.sizeMap.put(big, size1 + size2);
        this.sizeMap.remove(small);
    }


    public int size() {
        return this.sizeMap.size();
    }
}
