package com.msb.test;

import com.msb.model.TreeNode;

/**
 * Copyright (C), 2024-06-01
 *
 * @author bza
 * @version v1.0.0
 * @create TreeBalanced.java 2024-06-01 19:41
 */
public class TreeBalanced {

    public static void main(String[] args) {

    }

    public static boolean isBalanced(TreeNode root) {
        return doIsBalanced(root) != -1;
    }

    public static int doIsBalanced(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int left = doIsBalanced(root.left);
        int right = doIsBalanced(root.right);
        if (left == -1 || right == -1 || Math.abs(left - right) > 1) {
            return -1;
        }
        return Math.max(left, right) + 1;
    }
}
