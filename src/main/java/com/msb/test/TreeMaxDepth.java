package com.msb.test;

import com.msb.model.TreeNode;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Copyright (C), 2024-06-01
 *
 * @author bza
 * @version v1.0.0
 * @create TreeMaxDepth.java 2024-06-01 19:13
 */
public class TreeMaxDepth {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);

        TreeNode left = new TreeNode(2);
        TreeNode right = new TreeNode(2);

        root.left = left;
        root.right = right;

        left.left = new TreeNode(3);
        left.right = new TreeNode(4);

        right.left = new TreeNode(4);
        right.right = new TreeNode(3);

        System.out.println(maxDepth(root));
        System.out.println(maxDepth_v2(root));
    }

    public static int maxDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return Math.max(maxDepth(root.left), maxDepth(root.right)) + 1;
    }

    public static int maxDepth_v2(TreeNode root) {
        if (root == null) {
            return 0;
        }
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        int maxDepth = 0;
        while (!queue.isEmpty()) {
            int size = queue.size();
            while (size > 0) {
                TreeNode node = queue.poll();
                if (node.left != null) {
                    queue.offer(node.left);
                }
                if (node.right != null) {
                    queue.offer(node.right);
                }
                size--;
            }
            maxDepth++;
        }
        return maxDepth;
    }
}
