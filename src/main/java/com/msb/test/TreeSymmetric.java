package com.msb.test;

import com.msb.model.TreeNode;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Copyright (C), 2024-06-01
 *
 * @author bza
 * @version v1.0.0
 * @create TreeSymmetric.java 2024-06-01 18:51
 */
public class TreeSymmetric {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);

        TreeNode left = new TreeNode(2);
        TreeNode right = new TreeNode(2);

        root.left = left;
        root.right = right;

        left.left = new TreeNode(3);
        left.right = new TreeNode(4);

        right.left = new TreeNode(4);
        //right.right = new TreeNode(3);

        System.out.println(isSymmetric(root));
        System.out.println(isSymmetric_v2(root));
    }

    public static boolean isSymmetric(TreeNode root) {
        return doIsSymmetric(root.left, root.right);
    }

    public static boolean doIsSymmetric(TreeNode left, TreeNode right) {
        if (left == null && right == null) {
            return true;
        }
        if (left == null || right == null) {
            return false;
        }
        if (left.val != right.val) {
            return false;
        }
        return doIsSymmetric(left.left, right.right) && doIsSymmetric(left.right, right.left);
    }

    public static boolean isSymmetric_v2(TreeNode root) {
        if (root.left == null && root.right == null) {
            return true;
        }
        if (root.left == null || root.right == null) {
            return false;
        }
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root.left);
        queue.offer(root.right);

        while (!queue.isEmpty()) {
            TreeNode left = queue.poll();
            TreeNode right = queue.poll();
            if (left == null && right == null) {
                continue;
            }
            if (left == null || right == null) {
                return false;
            }
            queue.offer(left.left);
            queue.offer(right.right);

            queue.offer(left.right);
            queue.offer(right.left);
        }
        return true;
    }
}
