package com.msb.test;

import com.msb.model.TreeNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Copyright (C), 2024-06-01
 *
 * @author bza
 * @version v1.0.0
 * @create TreeTraversal.java 2024-06-01 15:39
 */
public class TreeTraversal {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(0);

        TreeNode left = new TreeNode(1);
        TreeNode right = new TreeNode(5);

        root.left = left;
        root.right = right;

        TreeNode leftLeft = new TreeNode(2);
        TreeNode leftRight = new TreeNode(4);

        left.left = leftLeft;
        left.right = leftRight;

        TreeNode rightLeft = new TreeNode(6);
        right.left = rightLeft;

        TreeNode leftLeftLeft = new TreeNode(2);
        TreeNode leftLeftRight = new TreeNode(4);

        rightLeft.left = leftLeftLeft;
        rightLeft.right = leftLeftRight;

        System.out.println(inorderTraversal(root));
        System.out.println(inorderTraversal_v2(root));

        System.out.println(preorderTraversal(root));
        System.out.println(preorderTraversal_v2(root));

        System.out.println(postorderTraversal(root));
        System.out.println(postorderTraversal_v2(root));
    }

    public static List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> collectList = new ArrayList<>();
        doInorderTraversal(root, collectList);
        return collectList;
    }

    private static void doInorderTraversal(TreeNode root, List<Integer> collectList) {
        if (root == null) {
            return;
        }
        doInorderTraversal(root.left, collectList);
        collectList.add(root.val);
        doInorderTraversal(root.right, collectList);
    }

    public static List<Integer> inorderTraversal_v2(TreeNode root) {
        List<Integer> collectList = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();
        while (root != null || !stack.isEmpty()) {
            while (root != null) {
                stack.push(root);
                root = root.left;
            }
            root = stack.pop();
            collectList.add(root.val);
            root = root.right;
        }
        return collectList;
    }

    public static List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> collectList = new ArrayList<>();
        doPreorderTraversal(root, collectList);
        return collectList;
    }

    private static void doPreorderTraversal(TreeNode root, List<Integer> collectList) {
        if (root == null) {
            return;
        }
        collectList.add(root.val);
        doPreorderTraversal(root.left, collectList);
        doPreorderTraversal(root.right, collectList);
    }

    public static List<Integer> preorderTraversal_v2(TreeNode root) {
        List<Integer> collectList = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();
        while (root != null || !stack.isEmpty()) {
            while (root != null) {
                collectList.add(root.val);
                stack.push(root);
                root = root.left;
            }
            root = stack.pop();
            root = root.right;
        }
        return collectList;
    }

    public static List<Integer> postorderTraversal(TreeNode root) {
        List<Integer> collectList = new ArrayList<>();
        doPostorderTraversal(root, collectList);
        return collectList;
    }

    private static void doPostorderTraversal(TreeNode root, List<Integer> collectList) {
        if (root == null) {
            return;
        }
        doPostorderTraversal(root.left, collectList);
        doPostorderTraversal(root.right, collectList);
        collectList.add(root.val);
    }

    public static List<Integer> postorderTraversal_v2(TreeNode root) {
        List<Integer> collectList = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();
        TreeNode preRoot = null;
        while (root != null || !stack.isEmpty()) {
            while (root != null) {
                stack.push(root);
                root = root.left;
            }
            root = stack.pop();
            if (root.right == null || root.right == preRoot) {
                collectList.add(root.val);
                preRoot = root;
                root = null;
            } else {
                stack.push(root);
                root = root.right;
            }
        }
        return collectList;
    }
}
