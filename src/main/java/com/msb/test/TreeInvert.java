package com.msb.test;

import com.msb.model.TreeNode;

/**
 * Copyright (C), 2024-06-01
 *
 * @author bza
 * @version v1.0.0
 * @create TreeInvert.java 2024-06-01 20:42
 */
public class TreeInvert {

    public static void main(String[] args) {

    }

    public static TreeNode invertTree(TreeNode root) {
        if (root == null) {
            return null;
        }
        TreeNode right = root.right;
        root.right = invertTree(root.left);
        root.left = invertTree(right);
        return root;
    }
}
