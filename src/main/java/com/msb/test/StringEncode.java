package com.msb.test;

import java.util.Stack;

/**
 * Copyright (C), 2024-06-01
 *
 * @author bza
 * @version v1.0.0
 * @create StringEncode.java 2024-06-01 13:09
 */
public class StringEncode {

    public static void main(String[] args) {
        String str = "3[a]2[bc]";
        System.out.println(decodeString(str));

        str = "3[a2[c]]";
        System.out.println(decodeString(str));
    }

    public static String decodeString(String s) {
        // 输入：s = "3[a]2[bc]"
        // 输出："aaabcbc"

        // 输入：s = "3[a2[c]]"
        // 输出："accaccacc"

        Stack<Character> stringStack = new Stack<>();
        int length = s.length();
        int idx = 0;
        while (idx < length) {
            char pushChar = s.charAt(idx);
            if (']' != pushChar) {
                stringStack.push(pushChar);
            } else {
                Character popChar;
                StringBuilder builder = new StringBuilder();
                while ((popChar = stringStack.pop()) != '[') {
                    builder.append(popChar);
                }
                String charStr = builder.reverse().toString();
                builder.delete(0, builder.length());

                while (!stringStack.isEmpty() && Character.isDigit(stringStack.peek())) {
                    builder.append(stringStack.pop());
                }
                String curStr = builder.reverse().toString();
                builder.delete(0, builder.length());

                int num = Integer.parseInt(curStr);
                while (num > 0) {
                    builder.append(charStr);
                    num--;
                }
                for (int i = 0; i < builder.length(); i++) {
                    stringStack.push(builder.charAt(i));
                }
            }
            idx++;

        }
        StringBuilder resultBuilder = new StringBuilder();
        while (!stringStack.isEmpty()) {
            resultBuilder.append(stringStack.pop());
        }
        return resultBuilder.reverse().toString();
    }
}
