package com.msb.graph;

import java.util.*;

/**
 * @author bza625858
 * @version : Prim.java, v 0.1 2024-07-17 22:30 bza625858 Exp $$
 */
public class Prim {

    /**
     * 使用Prim算法从无向图中生成最小生成树。
     * Prim算法是一种贪心算法，用于找到无向图中最小生成树的最小权边集合。
     * 最小生成树是连接图中所有节点的树，其边的权重之和最小。
     *
     * @param graph 输入的无向图。
     * @return 返回一个边的集合，这些边构成了图的最小生成树。
     */
    public static Set<Graph.Edge> prim(Graph graph) {
        // 用于存储已经访问过的节点
        Set<Graph.Node> cache = new HashSet<>();
        // 用于存储最小生成树的边集合
        Set<Graph.Edge> result = new HashSet<>();
        // 优先队列，按照边的权重排序，用于选择最小权重的边
        PriorityQueue<Graph.Edge> queue = new PriorityQueue<>(Comparator.comparingInt(Graph.Edge::getWeight));

        // 遍历图中的所有节点
        for (Map.Entry<Integer, Graph.Node> entry : graph.nodes.entrySet()) {
            Graph.Node node = entry.getValue();
            // 如果节点已经访问过，则跳过
            if (cache.contains(node)) {
                continue;
            }
            // 将当前节点标记为已访问
            cache.add(node);
            // 将当前节点的所有边加入优先队列
            queue.addAll(node.edges);

            // 当优先队列不为空时，继续处理
            while (!queue.isEmpty()) {
                // 从优先队列中取出权重最小的边
                Graph.Edge miniEdge = queue.poll();
                Graph.Node toNode = miniEdge.to;
                // 如果边的另一端节点已经访问过，则忽略
                if (!cache.contains(toNode)) {
                    // 将未访问的节点标记为已访问
                    cache.add(toNode);
                    // 将该边加入最小生成树的边集合
                    result.add(miniEdge);
                    // 将未访问节点的所有边加入优先队列
                    queue.addAll(toNode.edges);
                }
            }
            // break;
        }
        // 返回最小生成树的边集合
        return result;
    }

}
