package com.msb.graph;

import java.util.*;

/**
 * @author bza625858
 * @version : Dijkstra.java, v 0.1 2024-07-17 23:23 bza625858 Exp $$
 */
public class Dijkstra {

    public static Map<Graph.Node, Integer> dijkstra(Graph.Node start) {
        Map<Graph.Node, Integer> distanceMap = new HashMap<>();
        distanceMap.put(start, 0);
        Set<Graph.Node> hashSet = new HashSet<>();

        Graph.Node miniDisNode;
        while ((miniDisNode = getUnSelectedMinDisNode(hashSet, distanceMap)) != null) {
            Integer distance = distanceMap.get(miniDisNode);
            for (Graph.Edge edge : miniDisNode.edges) {
                Graph.Node toNode = edge.to;
                if (!hashSet.contains(toNode)) {
                    distanceMap.put(toNode, distance + edge.weight);
                } else {
                    distanceMap.put(toNode, Math.min(distance + edge.weight, distanceMap.get(toNode)));
                }
            }
            hashSet.add(miniDisNode);
        }
        return distanceMap;
    }

    private static Graph.Node getUnSelectedMinDisNode(Set<Graph.Node> hashSet, Map<Graph.Node, Integer> distanceMap) {
        Graph.Node minDisNode = null;
        int minDis = Integer.MAX_VALUE;
        for (Map.Entry<Graph.Node, Integer> entry : distanceMap.entrySet()) {
            Graph.Node node = entry.getKey();
            int distance = entry.getValue();
            if (!hashSet.contains(node) && distance < minDis) {
                minDis = distance;
                minDisNode = node;
            }
        }
        return minDisNode;
    }


}
