package com.msb.graph;

import java.util.*;

/**
 * @author bza625858
 * @version : TopologicalSort.java, v 0.1 2024-07-17 21:32 bza625858 Exp $$
 */
public class TopologicalSort {

    /**
     * 对给定的图进行拓扑排序。
     * 拓扑排序是无向图中节点的一种排序方式，使得对于每条边的两个节点，如果一个节点排在另一个节点之前，
     * 那么这个节点不能在那个节点的前面。换句话说，拓扑排序消除了所有入度为1的节点的前置节点。
     *
     * @param graph 输入的图，必须是没有循环的无向图。
     * @return 返回一个节点的列表，表示拓扑排序的结果。
     */
    public static List<Graph.Node> topologicalSort(Graph graph) {
        // 使用哈希表存储每个节点的入度
        Map<Graph.Node, Integer> inMap = new HashMap<>();
        // 使用队列存储入度为0的节点，这些节点可以作为排序的起点
        Queue<Graph.Node> zeroQueue = new LinkedList<>();

        // 遍历图中的所有节点，初始化入度信息，并将入度为0的节点加入队列
        graph.nodes.forEach((key, node) -> {
            inMap.put(node, node.in);
            if (node.in == 0) {
                // 入度为0的节点
                zeroQueue.add(node);
            }
        });

        // 用于存储拓扑排序的结果
        List<Graph.Node> result = new ArrayList<>();
        // 当队列不为空时，继续进行排序
        while (!zeroQueue.isEmpty()) {
            // 从队列中取出一个入度为0的节点
            Graph.Node node = zeroQueue.poll();
            result.add(node);
            // 遍历该节点的所有后续节点，减少它们的入度
            for (Graph.Node next : node.nexts) {
                inMap.put(next, inMap.get(next) - 1);
                // 如果某个节点的入度减少到0，则将其加入队列
                if (inMap.get(next) == 0) {
                    // 入度为0的节点
                    zeroQueue.add(next);
                }
            }
        }
        return result;
    }

}
