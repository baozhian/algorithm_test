package com.msb.graph;

import java.util.*;

/**
 * @author bza625858
 * @version : Kruskal.java, v 0.1 2024-07-17 22:30 bza625858 Exp $$
 */
public class Kruskal {

    /**
     * 使用Kruskal算法找到图的最小生成树。
     * Kruskal算法是一种贪心算法，用于找到连接所有节点的最小权重边集合，这些边构成了一个树，即最小生成树。
     *
     * @param graph 输入的图，必须是连通图。
     * @return 返回一个边的集合，这些边构成了图的最小生成树。
     */
    public static Set<Graph.Edge> kruskal(Graph graph) {
        // 使用并查集数据结构来判断两个节点是否属于同一个连通分量
        UnionFind unionFind = new UnionFind(graph.nodes.values());

        // 初始化待处理的边集合，即图中所有的边
        Set<Graph.Edge> edges = graph.edges;
        // 使用优先队列对边进行排序，排序依据是边的权重
        PriorityQueue<Graph.Edge> queue = new PriorityQueue<>(Comparator.comparingInt(Graph.Edge::getWeight));
        queue.addAll(edges);

        // 用于收集最小生成树的边集合
        Set<Graph.Edge> collect = new HashSet<>();
        while (!queue.isEmpty()) {
            // 从优先队列中取出权重最小的边
            Graph.Edge edge = queue.poll();
            // 如果边的起点和终点不属于同一个连通分量，则该边可以加入最小生成树
            if (!unionFind.isSameSet(edge.from, edge.to)) {
                collect.add(edge);
                // 将边的起点和终点合并为同一个连通分量
                unionFind.merge(edge.from, edge.to);
            }
        }
        return collect;
    }


    private static class UnionFind {
        private Map<Graph.Node, Graph.Node> parent;
        private Map<Graph.Node, Integer> size;

        public UnionFind(Collection<Graph.Node> collection) {
            this.parent = new HashMap<>();
            this.size = new HashMap<>();
            for (Graph.Node node : collection) {
                this.parent.put(node, node);
                this.size.put(node, 1);
            }
        }

        private Graph.Node findFather(Graph.Node cur) {
            Stack<Graph.Node> stack = new Stack<>();
            while (cur != this.parent.get(cur)) {
                stack.add(cur);
                cur = this.parent.get(cur);
            }
            while (!stack.isEmpty()) {
                this.parent.put(stack.pop(), cur);
            }
            return cur;
        }

        public boolean isSameSet(Graph.Node a, Graph.Node b) {
            return this.findFather(a) == this.findFather(b);
        }

        public void merge(Graph.Node a, Graph.Node b) {
            Graph.Node node1 = this.findFather(a);
            Graph.Node node2 = this.findFather(b);
            if (node1 == node2) {
                return;
            }
            int size1 = this.size.get(node1);
            int size2 = this.size.get(node2);
            if (size1 >= size2) {
                this.parent.put(node2, node1);
                this.size.put(node1, size1 + size2);
                this.size.remove(node2);
            } else {
                this.parent.put(node1, node2);
                this.size.put(node2, size1 + size2);
                this.size.remove(node1);
            }
        }

    }
}
