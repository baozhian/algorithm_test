package com.msb.graph;

import java.util.*;

/**
 * Copyright (C), 2024-07-14
 *
 * @author bza
 * @version v1.0.0
 * @create Graph.java 2024-07-14 13:36
 */
public class Graph {
    public Map<Integer, Node> nodes;
    public Set<Edge> edges;

    public Graph() {
        nodes = new HashMap<>();
        edges = new HashSet<>();
    }

    public static class Node {
        public String value;
        public int in;
        public int out;
        public List<Node> nexts;
        public List<Edge> edges;

        public Node(String value) {
            this.value = value;
            this.in = 0;
            this.out = 0;
            this.nexts = new ArrayList<>();
            this.edges = new ArrayList<>();
        }
    }

    public static class Edge {
        public int weight;
        public Node from;
        public Node to;

        public Edge(int weight, Node from, Node to) {
            this.weight = weight;
            this.from = from;
            this.to = to;
        }

        public int getWeight() {
            return weight;
        }

        public Node getFrom() {
            return from;
        }

        public Node getTo() {
            return to;
        }
    }
}