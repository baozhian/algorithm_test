package com.msb.graph;

import java.util.*;

/**
 * Copyright (C), 2024-07-17
 *
 * @author bza
 * @version v1.0.0
 * @create GraphVisitor.java 2024-07-17 20:54
 */
public class GraphVisitor {

    public static void main(String[] args) {

        Graph.Node a = new Graph.Node("a");
        Graph.Node b = new Graph.Node("b");
        Graph.Node c = new Graph.Node("c");

        a.nexts.add(b);
        a.nexts.add(c);

        Graph.Node k = new Graph.Node("k");
        Graph.Node e = new Graph.Node("e");
        Graph.Node f = new Graph.Node("f");

        b.nexts.add(c);
        b.nexts.add(k);

        k.nexts.add(e);

        c.nexts.add(e);
        c.nexts.add(f);

        e.nexts.add(f);

        breathFirstSearch(a);
        System.out.println("");
        depthFirstSearch(a);
    }


    /**
     * 实现广度优先搜索（BFS）遍历图。
     * 该方法从指定的起始节点开始，按照广度优先的顺序遍历图中的节点。
     * 广度优先搜索的特点是先访问距离起点近的节点，再访问距离起点远的节点。
     *
     * @param start 图的起始节点，即搜索的起点。
     */
    public static void breathFirstSearch(Graph.Node start) {
        // 用于存储已访问过的节点，避免重复访问
        Set<Graph.Node> cache = new HashSet<>();

        // 使用队列来存储待访问的节点，队列的特性保证了广度优先的访问顺序
        Queue<Graph.Node> queue = new LinkedList<>();
        // 将起始节点加入队列和已访问节点集合
        queue.add(start);
        cache.add(start);

        // 当队列不为空时，继续遍历
        while (!queue.isEmpty()) {
            // 出队列一个节点，并进行访问
            Graph.Node cur = queue.poll();
            System.out.print(cur.value + " ");

            // 遍历当前节点的所有相邻节点
            for (Graph.Node next : cur.nexts) {
                // 如果相邻节点未被访问过，则将其加入队列和已访问节点集合
                if (!cache.contains(next)) {
                    queue.add(next);
                    cache.add(next);
                }
            }
        }
    }



    /**
     * 使用深度优先搜索(DFS)遍历图。
     * 从给定的起始节点开始，该方法将递归地探索图中的节点，直到访问完所有可达节点。
     * 使用栈来实现DFS的回溯行为，确保每个节点只被访问一次。
     *
     * @param start 遍历的起始节点。
     */
    public static void depthFirstSearch(Graph.Node start) {
        // 使用HashSet存储已访问的节点，避免重复访问
        Set<Graph.Node> cache = new HashSet<>();
        // 使用栈来存储待访问的节点
        Stack<Graph.Node> stack = new Stack<>();
        // 将起始节点加入栈和已访问节点集合
        stack.add(start);
        cache.add(start);
        // 输出起始节点值
        System.out.print(start.value + " ");

        // 当栈不为空时，继续遍历
        while (!stack.isEmpty()) {
            // 弹出栈顶节点，即当前正在处理的节点
            Graph.Node cur = stack.pop();
            // 遍历当前节点的相邻节点
            for (Graph.Node next : cur.nexts) {
                // 如果相邻节点未被访问过，则将其加入栈和已访问节点集合
                if (!cache.contains(next)) {
                    // 先将当前节点重新压入栈中，以便后续回溯
                    stack.add(cur);
                    // 将相邻节点压入栈中，作为下一个处理的节点
                    stack.add(next);
                    cache.add(next);
                    // 输出相邻节点值
                    System.out.print(next.value + " ");
                    // 找到一个未访问的相邻节点后，即可跳出循环，处理下一个相邻节点
                    break;
                }
            }
        }
    }

}
