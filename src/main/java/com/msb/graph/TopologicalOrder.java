package com.msb.graph;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author bza625858
 * @version : TopologicalOrder.java, v 0.1 2024-07-17 21:55 bza625858 Exp $$
 */
public class TopologicalOrder {

    public static class DirectedGraphNode {
        public int label;
        public List<DirectedGraphNode> neighbors;

        public DirectedGraphNode(int x) {
            label = x;
            neighbors = new ArrayList<>();
        }
    }

    private static class Record {
        int count;
        DirectedGraphNode node;

        public Record(DirectedGraphNode node, int count) {
            this.node = node;
            this.count = count;
        }

        public int getCount() {
            return count;
        }

        public DirectedGraphNode getNode() {
            return node;
        }
    }

    /**
     * 对有向无环图进行深度优先搜索，并返回节点的排序列表。
     * 该方法首先通过遍历所有图中的节点，记录每个节点的入度和访问状态。
     * 然后根据节点的入度进行排序，返回入度为0的节点列表，这些节点构成了排序的起点。
     *
     * @param graph 有向无环图的节点列表
     * @return 排序后的节点列表
     */
    public static List<DirectedGraphNode> topSort(List<DirectedGraphNode> graph) {
        // 使用哈希表缓存每个节点的入度和访问状态
        Map<DirectedGraphNode, Record> cache = new HashMap<>();
        // 用于收集所有节点的记录
        List<Record> collect = new ArrayList<>();
        // 遍历图中的每个节点
        for (DirectedGraphNode graphNode : graph) {
            // 处理节点，更新其入度和访问状态，并返回记录
            Record record = process(graphNode, cache);
            // 将节点的记录添加到收集列表中
            collect.add(record);
        }
        // 根据节点的入度进行排序，并返回排序后的节点列表
        return collect.stream()
                .sorted(Comparator.comparingInt(Record::getCount))
                .map(Record::getNode)
                .collect(Collectors.toList());
    }


    /**
     * 处理给定节点并计算其入度。
     * 该方法使用递归方式遍历图中的节点，通过累加邻居节点的入度来计算当前节点的入度。
     * 使用缓存（Map）来存储已经计算过的节点的入度，避免重复计算。
     *
     * @param node  当前处理的节点。
     * @param cache 存储已经计算过入度的节点及其入度的缓存。
     * @return 当前节点的入度记录。
     */
    private static Record process(DirectedGraphNode node, Map<DirectedGraphNode, Record> cache) {
        // 检查缓存中是否已经有当前节点的入度记录，如果有则直接返回
        if (cache.containsKey(node)) {
            return cache.get(node);
        }
        // 初始化当前节点的入度计数为0
        int count = 0;
        // 遍历当前节点的所有邻居节点，累加它们的入度到当前节点的入度计数中
        for (DirectedGraphNode neighbor : node.neighbors) {
            count += process(neighbor, cache).count;
        }
        Record record = new Record(node, count + 1);
        cache.put(node, record);
        // 创建并返回当前节点的入度记录
        return record;
    }

}
