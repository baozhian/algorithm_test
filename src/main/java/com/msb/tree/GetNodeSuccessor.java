package com.msb.tree;

/**
 * @author bza625858
 * @version : GetNodeSuccessor.java, v 0.1 2024-07-04 21:30 bza625858 Exp $$
 */
public class GetNodeSuccessor {

    public static class Node {
        Node left;
        Node right;
        int val;
        Node parent;
    }

    public static Node getNodeSuccessor(Node node) {
        if (node == null) {
            return null;
        }
        Node rightLeft = node.right;
        if (rightLeft != null) {
            while (rightLeft.left != null) {
                rightLeft = rightLeft.left;
            }
            return rightLeft;
        }
        Node parent = node.parent;
        while (parent != null && parent.left != node) {
            node = parent;
            parent = node.parent;
        }
        return parent;
    }
}
