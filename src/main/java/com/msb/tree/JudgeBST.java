package com.msb.tree;

import com.msb.model.TreeNode;

/**
 * @author bza625858
 * @version : JudgeBST.java, v 0.1 2024-07-08 21:17 bza625858 Exp $$
 */
public class JudgeBST {

    private static class Info {
        boolean bST;
        int max;
        int min;

        public Info(boolean bST, int max, int min) {
            this.bST = bST;
            this.max = max;
            this.min = min;
        }
    }

    public static boolean isBST(TreeNode root) {
        if (root == null) {
            return true;
        }
        return process(root).bST;
    }

    private static Info process(TreeNode root) {
        if (root == null) {
            return null;
        }
        boolean bST = true;
        int max = root.val;
        int min = root.val;
        Info leftInfo = process(root.left);
        Info rightInfo = process(root.right);
        if (leftInfo != null) {
            max = Math.max(leftInfo.max, max);
            min = Math.min(leftInfo.min, min);
            if (leftInfo.max >= root.val) {
                bST = false;
            }
        }
        if (rightInfo != null) {
            max = Math.max(rightInfo.max, max);
            min = Math.min(rightInfo.min, min);
            if (rightInfo.min <= root.val) {
                bST = false;
            }
        }
        return new Info(bST, max, min);
    }

}
