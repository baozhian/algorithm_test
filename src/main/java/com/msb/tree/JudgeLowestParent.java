package com.msb.tree;

import com.msb.model.TreeNode;

/**
 * @author bza625858
 * @version : JudgeLowestParent.java, v 0.1 2024-07-09 23:30 bza625858 Exp $$
 */
public class JudgeLowestParent {

    public static TreeNode lowestAncestor1(TreeNode head, TreeNode a, TreeNode b) {
        return process(head, a, b).parent;
    }

    private static Info process(TreeNode head, TreeNode a, TreeNode b) {
        if (head == null) {
            return new Info(false, false, null);
        }
        Info leftInfo = process(head.left, a, b);
        Info rightInfo = process(head.right, a, b);

        boolean findA = (head == a) || leftInfo.findA || rightInfo.findA;
        boolean findB = (head == b) || leftInfo.findB || rightInfo.findB;
        TreeNode parent = null;
        if (leftInfo.parent != null) {
            parent = leftInfo.parent;
        } else if (rightInfo.parent != null) {
            parent = rightInfo.parent;
        } else if (findA && findB) {
            parent = head;
        }
        return new Info(findA, findB, parent);
    }


    private static class Info {
        boolean findA;
        boolean findB;
        TreeNode parent;

        public Info(boolean findA, boolean findB, TreeNode parent) {
            this.findA = findA;
            this.findB = findB;
            this.parent = parent;
        }
    }
}
