package com.msb.tree;

import com.msb.model.TreeNode;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author bza625858
 * @version : TreeMaxWidth.java, v 0.1 2024-07-02 21:32 bza625858 Exp $$
 */
public class TreeMaxWidth {

    /**
     * 二叉树最大宽度
     */
    public static int getTreeMaxWidth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);

        int curWidth = 0;
        int maxWidth = 0;
        TreeNode curEndNode = root;
        TreeNode nextEndNode = null;
        while (!queue.isEmpty()) {
            TreeNode cur = queue.poll();
            if (cur.left != null) {
                queue.add(cur.left);
                nextEndNode = cur.left;
            }
            if (cur.right != null) {
                queue.add(cur.right);
                nextEndNode = cur.right;
            }
            curWidth++;
            if (cur == curEndNode) {
                maxWidth = Math.max(maxWidth, curWidth);
                curWidth = 0;
                curEndNode = nextEndNode;
            }
        }
        return maxWidth;
    }

}
