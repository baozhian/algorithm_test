package com.msb.tree;

/**
 * @author bza625858
 * @version : PagerFolding.java, v 0.1 2024-07-05 22:27 bza625858 Exp $$
 */
public class PagerFolding {

    public static void printAllFolding(int num) {
        process(1, num, true);
    }

    private static void process(int i, int num, boolean down) {
        if (i > num) {
            return;
        }
        process(i + 1, num, true);
        System.out.print(down ? "凹 " : "凸 ");
        process(i + 1, num, false);
    }
}
