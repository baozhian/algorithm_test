package com.msb.tree;

import com.msb.model.TreeNode;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author bza625858
 * @version : JudgeCBT.java, v 0.1 2024-07-06 13:03 bza625858 Exp $$
 */
public class JudgeCBT {

    public static boolean isCBT(TreeNode root) {
        if (root == null) {
            return true;
        }
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        boolean isLeaf = false;
        while (!queue.isEmpty()) {
            TreeNode node = queue.poll();
            TreeNode left = node.left;
            TreeNode right = node.right;
            if ((left == null && right != null) || (isLeaf && (left != null || right != null))) {
                return false;
            }
            if (left != null) {
                queue.add(left);
            }
            if (right != null) {
                queue.add(right);
            }
            if (left == null || right == null) {
                isLeaf = true;
            }
        }
        return true;
    }

    private static class Info {
        boolean isFBT;
        boolean isCBT;
        int height;

        public Info(boolean isFBT, boolean isCBT, int height) {
            this.isFBT = isFBT;
            this.isCBT = isCBT;
            this.height = height;
        }
    }

    public static boolean isCBT_v2(TreeNode root) {
        return process(root).isCBT;
    }

    private static Info process(TreeNode node) {
        if (node == null) {
            return new Info(true, true, 0);
        }
        Info leftInfo = process(node.left);
        Info rightInfo = process(node.right);
        boolean isFBT = leftInfo.isFBT && rightInfo.isFBT && leftInfo.height == rightInfo.height;
        boolean isCBT = false;
        if (isFBT) {
            isCBT = true;
        } else if (leftInfo.isFBT && rightInfo.isFBT && leftInfo.height == rightInfo.height + 1) {
            isCBT = true;
        } else if (leftInfo.isCBT && rightInfo.isFBT && leftInfo.height == rightInfo.height + 1) {
            isCBT = true;
        } else if (leftInfo.isFBT && rightInfo.isCBT && leftInfo.height == rightInfo.height) {
            isCBT = true;
        }
        int height = Math.max(leftInfo.height, rightInfo.height) + 1;
        return new Info(isFBT, isCBT, height);
    }
}
