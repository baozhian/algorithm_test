package com.msb.tree;

import com.msb.model.TreeNode;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

/**
 * @author bza625858
 * @version : TreeSerialize.java, v 0.1 2024-06-26 22:01 bza625858 Exp $$
 */
public class TreeSerialize {

    public static void main(String[] args) {
        Integer[] arr = {1, 3, 2, 5, 3, 9, null, 15, 16};
        TreeNode root = TreeNode.arrayToBTree(arr);
        TreeNode.treePrint(root);

        System.out.println("------------------------------");
        Queue<Integer> queue = preOrderSerialize(root);
        Integer[] integers = queue.toArray(Integer[]::new);
        System.out.println(Arrays.toString(integers));
        TreeNode node = preOrderDeSerialize(queue);
        TreeNode.treePrint(node);

        System.out.println("------------------------------");

        queue = levelSerialize(root);
        integers = queue.toArray(Integer[]::new);
        System.out.println(Arrays.toString(integers));
        node = levelDeSerialize(queue);
        TreeNode.treePrint(node);
    }

    public static Queue<Integer> preOrderSerialize(TreeNode root) {
        Queue<Integer> queue = new LinkedList<>();
        doPreOrderSerial(root, queue);
        return queue;
    }

    private static void doPreOrderSerial(TreeNode root, Queue<Integer> queue) {
        if (root == null) {
            queue.add(null);
        } else {
            queue.add(root.val);
            doPreOrderSerial(root.left, queue);
            doPreOrderSerial(root.right, queue);
        }
    }

    public static TreeNode preOrderDeSerialize(Queue<Integer> queue) {
        if (queue == null || queue.isEmpty()) {
            return null;
        }
        return doPreOrderDeSerial(queue);
    }

    private static TreeNode doPreOrderDeSerial(Queue<Integer> queue) {
        Integer integer = queue.poll();
        if (integer == null) {
            return null;
        }
        TreeNode node = new TreeNode(integer);
        node.left = doPreOrderDeSerial(queue);
        node.right = doPreOrderDeSerial(queue);
        return node;
    }


    public static Queue<Integer> levelSerialize(TreeNode root) {
        Queue<Integer> store = new LinkedList<>();
        if (root == null) {
            store.add(null);
        } else {
            store.add(root.val);
            Queue<TreeNode> queue = new LinkedList<>();
            queue.add(root);
            while (!queue.isEmpty()) {
                TreeNode node = queue.poll();
                if (node.left != null) {
                    store.add(node.left.val);
                    queue.add(node.left);
                } else {
                    store.add(null);
                }
                if (node.right != null) {
                    store.add(node.right.val);
                    queue.add(node.right);
                } else {
                    store.add(null);
                }
            }
        }
        return store;
    }


    public static TreeNode levelDeSerialize(Queue<Integer> store) {
        if (store == null || store.isEmpty()) {
            return null;
        }
        Queue<TreeNode> queue = new LinkedList<>();
        TreeNode root = new TreeNode(store.poll());
        queue.add(root);
        while (!queue.isEmpty()) {
            TreeNode node = queue.poll();
            node.left = generateNode(store.poll());
            if (node.left  != null) {
                queue.add(node.left);
            }
            node.right =  generateNode(store.poll());
            if (node.right != null) {
                queue.add(node.right);
            }
        }
        return root;
    }

    public static TreeNode generateNode(Integer val) {
        if (val == null) {
            return null;
        }
        return new TreeNode(val);
    }

}
