package com.msb.tree;

import com.msb.model.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author bza625858
 * @version : MultiTreeToBinaryTree.java, v 0.1 2024-07-01 22:57 bza625858 Exp $$
 */
public class MultiTreeToBinaryTree {

    private static class Node {

        private Integer val;

        private List<Node> children;

        public Node(Integer val, List<Node> children) {
            this.val = val;
            this.children = children;
        }
    }

    public static TreeNode encode(Node root) {
        if (root == null) {
            return null;
        }
        TreeNode head = new TreeNode(root.val);
        head.left = doEncode(root.children);
        return head;
    }

    private static TreeNode doEncode(List<Node> children) {
        if (children == null) {
            return null;
        }
        TreeNode first = null;
        TreeNode cur = null;
        for (Node child : children) {
            TreeNode treeNode = new TreeNode(child.val);
            if (first == null) {
                first = treeNode;
            } else {
                cur.right = treeNode;
            }
            cur = treeNode;
            cur.left = doEncode(child.children);
        }
        return first;
    }


    public static Node decode(TreeNode root) {
        if (root == null) {
            return null;
        }
        return new Node(root.val, doEncode(root.left));
    }

    private static List<Node> doEncode(TreeNode child) {
        List<Node> children = null;
        while (child != null) {
            Node node = new Node(child.val, doEncode(child.left));
            if (children == null) {
                children = new ArrayList<>();
            } else {
                children.add(node);
            }
            child = child.right;
        }
        return children;
    }
}
