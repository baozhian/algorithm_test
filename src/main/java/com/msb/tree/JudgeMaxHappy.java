package com.msb.tree;

import java.util.List;

/**
 * @author bza625858
 * @version : JudgeMaxHappy.java, v 0.1 2024-07-09 23:36 bza625858 Exp $$
 */
public class JudgeMaxHappy {

    private static class Emp {
        int happy;
        List<Emp> nexts;

        public Emp(int happy, List<Emp> nexts) {
            this.happy = happy;
            this.nexts = nexts;
        }
    }

    private static class Info {
        int yes;
        int no;

        public Info(int yes, int no) {
            this.yes = yes;
            this.no = no;
        }
    }

    public static int getMaxHappy(Emp root) {
        Info info = process(root);
        return Math.max(info.yes, info.no);
    }

    private static Info process(Emp root) {
        if (root == null) {
            return new Info(0, 0);
        }
        int yes = root.happy;
        int no = 0;
        for (Emp next : root.nexts) {
            Info info = process(next);
            yes += info.no;
            no += Math.max(info.yes, info.no);
        }
        return new Info(yes, no);
    }
}
