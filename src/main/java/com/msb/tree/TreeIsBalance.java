package com.msb.tree;

import com.msb.model.TreeNode;

/**
 * @author bza625858
 * @version : TreeIsBalance.java, v 0.1 2024-07-07 16:09 bza625858 Exp $$
 */
public class TreeIsBalance {

    private static class Info {
        int height;
        boolean balance;

        public Info(int height, boolean balance) {
            this.height = height;
            this.balance = balance;
        }
    }

    public static boolean isBalance(TreeNode root) {
        return process(root).balance;
    }

    private static Info process(TreeNode root) {
        if (root == null) {
            return new Info(0, true);
        }
        boolean balance = true;
        Info leftInfo = process(root.left);
        Info rightInfo = process(root.right);
        int height = Math.max(leftInfo.height, rightInfo.height) + 1;

        if (!leftInfo.balance || !rightInfo.balance || Math.abs(leftInfo.height - rightInfo.height) > 1) {
            balance = false;
        }
        return new Info(height, balance);
    }
}
