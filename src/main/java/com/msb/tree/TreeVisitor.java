package com.msb.tree;

import com.msb.model.TreeNode;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/**
 * @author bza625858
 * @version : TreeVisitor.java, v 0.1 2024-06-26 21:18 bza625858 Exp $$
 */
public class TreeVisitor {

    public static void main(String[] args) {
        Integer[] arr = {1, 3, 2, 5, 3, 9, null, 15, 16};
        TreeNode root = TreeNode.arrayToBTree(arr);
        TreeNode.treePrint(root);

        preOrderTree(root);
        System.out.println();
        preOrderTree_(root);
        System.out.println();
        System.out.println("----------------------------");

        inOrderTree(root);
        System.out.println();
        inOrderTree_(root);
        System.out.println();
        System.out.println("----------------------------");

        postOrderTree(root);
        System.out.println();
        postOrderTree_(root);
        System.out.println();
        System.out.println("----------------------------");

        floorOrderTree(root);
    }

    public static void preOrderTree(TreeNode root) {
        System.out.print(root.val + " -> ");
        if (root.left != null) {
            preOrderTree(root.left);
        }
        if (root.right != null) {
            preOrderTree(root.right);
        }
    }

    public static void inOrderTree(TreeNode root) {
        if (root.left != null) {
            inOrderTree(root.left);
        }
        System.out.print(root.val + " -> ");
        if (root.right != null) {
            inOrderTree(root.right);
        }
    }

    public static void postOrderTree(TreeNode root) {
        if (root.left != null) {
            postOrderTree(root.left);
        }
        if (root.right != null) {
            postOrderTree(root.right);
        }
        System.out.print(root.val + " -> ");
    }

    public static void preOrderTree_(TreeNode root) {
        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()) {
            TreeNode node = stack.pop();
            System.out.print(node.val + " -> ");
            if (node.right != null) {
                stack.push(node.right);
            }
            if (node.left != null) {
                stack.push(node.left);
            }
        }
    }

    public static void inOrderTree_(TreeNode root) {
        Stack<TreeNode> stack = new Stack<>();
        while (!stack.isEmpty() || root != null) {
            if (root != null) {
                stack.push(root);
                root = root.left;
            } else {
                TreeNode node = stack.pop();
                System.out.print(node.val + " -> ");
                root = node.right;
            }
        }
    }

    public static void postOrderTree_(TreeNode root) {
        Stack<TreeNode> stack = new Stack<>();
        Stack<TreeNode> store = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()) {
            TreeNode node = stack.pop();
            store.push(node);
            if (node.left != null) {
                stack.push(node.left);
            }
            if (node.right != null) {
                stack.push(node.right);
            }
        }
        while (!store.isEmpty()) {
            System.out.print(store.pop().val + " -> ");
        }
    }

    public static void floorOrderTree(TreeNode root) {
        Queue<TreeNode> stack = new LinkedList<>();
        stack.offer(root);
        while (!stack.isEmpty()) {
            TreeNode node = stack.poll();
            System.out.print(node.val + " -> ");
            if (node.left != null) {
                stack.offer(node.left);
            }
            if (node.right != null) {
                stack.offer(node.right);
            }
        }
    }
}
