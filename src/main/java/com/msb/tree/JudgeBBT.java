package com.msb.tree;

import com.msb.model.TreeNode;

/**
 * @author bza625858
 * @version : JudgeBBT.java, v 0.1 2024-07-06 13:03 bza625858 Exp $$
 */
public class JudgeBBT {

    private static class Info {
        boolean balance;
        int height;

        public Info(boolean balance, int height) {
            this.balance = balance;
            this.height = height;
        }
    }

    public static boolean isBBT(TreeNode root) {
        if (root == null) {
            return true;
        }
        return process(root).balance;
    }

    private static Info process(TreeNode root) {
        if (root == null) {
            return new Info(true, 0);
        }
        Info leftInfo = process(root.left);
        Info rightInfo = process(root.right);
        boolean balance = true;
        if (!leftInfo.balance || !rightInfo.balance) {
            balance = false;
        }
        if (Math.abs(leftInfo.height - rightInfo.height) > 1) {
            balance = false;
        }
        int height = Math.max(leftInfo.height, rightInfo.height) + 1;
        return new Info(balance, height);
    }
}
