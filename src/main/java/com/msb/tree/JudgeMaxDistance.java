package com.msb.tree;

import com.msb.model.TreeNode;

/**
 * @author bza625858
 * @version : JudgeMaxDistance.java, v 0.1 2024-07-08 23:43 bza625858 Exp $$
 */
public class JudgeMaxDistance {

    private static class Info {
        int maxDistance;
        int height;

        public Info(int maxDistance, int height) {
            this.maxDistance = maxDistance;
            this.height = height;
        }
    }

    public static int getMaxDistance(TreeNode root) {
        return doGetMaxDistance(root).maxDistance;
    }

    private static Info doGetMaxDistance(TreeNode root) {
        if (root == null) {
            return new Info(0, 0);
        }
        Info leftInfo = doGetMaxDistance(root.left);
        Info rightInfo = doGetMaxDistance(root.right);

        int maxDistance = Math.max(leftInfo.height, rightInfo.height);
        int height = maxDistance + 1;
        maxDistance = Math.max(maxDistance, leftInfo.height + rightInfo.height + 1);
        return new Info(maxDistance, height);
    }
}
