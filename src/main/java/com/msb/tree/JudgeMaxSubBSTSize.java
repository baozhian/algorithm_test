package com.msb.tree;

import com.msb.model.TreeNode;

/**
 * @author bza625858
 * @version : JudgeMaxSubBSTSize.java, v 0.1 2024-07-09 22:55 bza625858 Exp $$
 */
public class JudgeMaxSubBSTSize {


    public static int getMaxSubBSTSize(TreeNode root) {
        return process(root).maxSubBSTSize;
    }

    private static Info process(TreeNode root) {
        if (root == null) {
            return null;
        }
        Info leftInfo = process(root.left);
        Info rightInfo = process(root.right);

        int max = root.val;
        int min = root.val;
        int allSize = 1;
        int p1 = -1;
        boolean leftIsBST = false;
        if (leftInfo != null) {
            max = Math.max(max, leftInfo.max);
            min = Math.min(min, leftInfo.min);
            allSize += leftInfo.allSize;
            p1 = leftInfo.maxSubBSTSize;
            leftIsBST = leftInfo.max < root.val;
        }
        int p2 = -1;
        boolean rightIsBST = false;
        if (rightInfo != null) {
            max = Math.max(max, rightInfo.max);
            min = Math.min(min, rightInfo.min);
            allSize += rightInfo.allSize;
            p2 = rightInfo.maxSubBSTSize;
            rightIsBST = rightInfo.min > root.val;
        }
        int p3 = -1;
        if (leftIsBST && rightIsBST) {
            p3 = 1 + leftInfo.allSize + rightInfo.allSize;
        }
        int maxSubBSTSize = Math.max(p1, Math.max(p2, p3));
        return new Info(max, min, allSize, maxSubBSTSize);
    }

    private static class Info {
        int max;
        int min;
        int allSize;
        int maxSubBSTSize;

        public Info(int max, int min, int allSize, int maxSubBSTSize) {
            this.max = max;
            this.min = min;
            this.allSize = allSize;
            this.maxSubBSTSize = maxSubBSTSize;
        }
    }
}
