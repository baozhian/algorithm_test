package com.msb.tree;

import com.msb.model.TreeNode;

/**
 * @author bza625858
 * @version : JudgeMaxSubBSTHead.java, v 0.1 2024-07-09 22:55 bza625858 Exp $$
 */
public class JudgeMaxSubBSTHead {


    public static TreeNode getMaxSubBSTHead(TreeNode root) {
        return process(root).maxSubBSTHead;
    }

    private static Info process(TreeNode root) {
        if (root == null) {
            return null;
        }
        Info leftInfo = process(root.left);
        Info rightInfo = process(root.right);

        int max = root.val;
        int min = root.val;
        int maxSubBSTSize = 0;
        boolean leftIsBST = false;
        TreeNode maxSubBSTHead = null;
        if (leftInfo != null) {
            max = Math.max(max, leftInfo.max);
            min = Math.min(min, leftInfo.min);
            maxSubBSTSize = leftInfo.maxSubBSTSize;
            leftIsBST = leftInfo.max < root.val;
            maxSubBSTHead = leftInfo.maxSubBSTHead;
        }
        boolean rightIsBST = false;
        if (rightInfo != null) {
            max = Math.max(max, rightInfo.max);
            min = Math.min(min, rightInfo.min);
            if (rightInfo.maxSubBSTSize > maxSubBSTSize) {
                maxSubBSTSize = rightInfo.maxSubBSTSize;
                maxSubBSTHead = rightInfo.maxSubBSTHead;
            }
            rightIsBST = rightInfo.min > root.val;
        }
        if (leftIsBST && rightIsBST) {
            maxSubBSTHead = root;
            maxSubBSTSize = 1 + leftInfo.maxSubBSTSize + rightInfo.maxSubBSTSize;
        }
        return new Info(max, min, maxSubBSTSize, maxSubBSTHead);
    }

    private static class Info {
        int max;
        int min;
        int maxSubBSTSize;

        TreeNode maxSubBSTHead;

        public Info(int max, int min, int maxSubBSTSize, TreeNode maxSubBSTHead) {
            this.max = max;
            this.min = min;
            this.maxSubBSTSize = maxSubBSTSize;
            this.maxSubBSTHead = maxSubBSTHead;
        }
    }
}
