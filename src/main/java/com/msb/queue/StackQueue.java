package com.msb.queue;

import java.util.AbstractQueue;
import java.util.Iterator;
import java.util.Stack;

/**
 * Copyright (C), 2024-06-09
 *
 * @author bza
 * @version v1.0.0
 * @create StackQueue.java 2024-06-09 11:11
 */
public class StackQueue<E> extends AbstractQueue<E> {

    private Stack<E> writeStack;

    private Stack<E> readStack;

    public StackQueue() {
        this.writeStack = new Stack<>();
        this.readStack = new Stack<>();
    }

    @Override
    public Iterator<E> iterator() {
        return null;
    }

    @Override
    public int size() {
        while (!writeStack.isEmpty()) {
            readStack.push(writeStack.pop());
        }
        return readStack.size();
    }


    @Override
    public boolean offer(E e) {
        while (!writeStack.isEmpty()) {
            writeStack.push(writeStack.pop());
        }
        writeStack.push(e);
        return true;
    }

    @Override
    public E poll() {
        while (!writeStack.isEmpty()) {
            readStack.push(writeStack.pop());
        }
        return readStack.pop();
    }

    @Override
    public E peek() {
        while (!writeStack.isEmpty()) {
            readStack.push(writeStack.pop());
        }
        return readStack.peek();
    }

    @Override
    public String toString() {
        while (!writeStack.isEmpty()) {
            readStack.push(writeStack.pop());
        }
        StringBuilder builder = new StringBuilder();
        Iterator<E> iterator = readStack.iterator();
        while (iterator.hasNext()) {
            builder.append(iterator.next()).append(" -> ");
        }
        return builder.toString();
    }

    public static void main(String[] args) {
        StackQueue<Integer> queue = new StackQueue<>();
        queue.offer(1);
        queue.offer(2);
        System.out.println("添加1、2：" + queue);
        queue.offer(3);
        queue.offer(4);
        System.out.println("添加3、4：" + queue);

        System.out.println("取出一个(" + queue.poll() + ")：" + queue);
        queue.offer(5);
        queue.offer(6);
        System.out.println("添加5、6：" + queue);
        System.out.println("取出一个(" + queue.poll() + ")：" + queue);
        System.out.println("取出一个(" + queue.poll() + ")：" + queue);
        System.out.println("取出一个(" + queue.poll() + ")：" + queue);
        System.out.println("取出一个(" + queue.poll() + ")：" + queue);
        System.out.println("取出一个(" + queue.poll() + ")：" + queue);
        queue.offer(3);
        queue.offer(4);
        System.out.println("添加3、4：" + queue);
    }
}
