package com.msb.ext;

/**
 * Copyright (C), 2024-06-11
 *
 * @author bza
 * @version v1.0.0
 * @create ReversePair.java 2024-06-11 21:11
 */
public class ReversePair {

    public int process(Integer[] data) {
        return process(data, 0, data.length - 1);
    }

    private int process(Integer[] data, int start, int end) {
        if (start >= end) {
            return 0;
        }
        int middle = start + ((end - start) >> 1);
        return process(data, start, middle) + process(data, middle + 1, end) +
                merge(data, start, middle, end);
    }

    private int merge(Integer[] data, int start, int middle, int end) {
        Integer[] helper = new Integer[end - start + 1];
        int idx = helper.length - 1;
        int left = middle;
        int right = end;
        int result = 0;
        while (left >= start && right > middle) {
            result += data[left] > data[right] ? (right - middle) : 0;
            helper[idx--] = data[left] > data[right] ? data[left--] : data[right--];
        }
        while (left >= start) {
            helper[idx--] = data[left--];
        }
        while (right > middle) {
            helper[idx--] = data[right--];
        }
        for (int i = 0; i < helper.length; i++) {
            data[start + i] = helper[i];
        }
        return result;
    }
}
