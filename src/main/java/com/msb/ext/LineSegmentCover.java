package com.msb.ext;

import com.msb.model.LineSegment;

import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * Copyright (C), 2024-06-15
 *
 * @author bza
 * @version v1.0.0
 * @create LineSegmentCover.java 2024-06-15 16:14
 */
public class LineSegmentCover {

    public static int maxCover(LineSegment[] segments) {
        Arrays.sort(segments, Comparator.comparingInt(LineSegment::getStart));
        PriorityQueue<Integer> heap = new PriorityQueue<>();
        int max = 0;
        for (LineSegment segment : segments) {
            while (!heap.isEmpty() && heap.peek() <= segment.getStart()) {
                heap.poll();
            }
            heap.add(segment.getEnd());
            max = Math.max(heap.size(), max);
        }
        return max;
    }
}
