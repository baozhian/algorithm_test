package com.msb.ext;

/**
 * Copyright (C), 2024-06-11
 *
 * @author bza
 * @version v1.0.0
 * @create BigThenRightTwice.java 2024-06-11 20:49
 */
public class BigThenRightTwice {

    public int process(Integer[] data) {
        return process(data, 0, data.length - 1);
    }

    private int process(Integer[] data, int start, int end) {
        if (start >= end) {
            return 0;
        }
        int middle = start + ((end - start) >> 1);
        int result = process(data, start, middle);
        result += process(data, middle + 1, end);
        result += merge(data, start, middle, end);
        return result;
    }


    private int merge(Integer[] data, int start, int middle, int end) {
        int leftPtr = start;
        int rightPtr = middle + 1;
        Integer[] helper = new Integer[end - start + 1];
        int i = 0;
        int result = 0;
        for (int j = start; j <= end; j++) {
            int curPtr = rightPtr;
            while (curPtr <= end && data[j] > data[curPtr] * 2) {
                curPtr++;
            }
            int val = curPtr - rightPtr;
            result += val;
            System.out.printf("%d -> %d", data[j], val);
            System.out.println();
        }
        while (leftPtr <= middle && rightPtr <= end) {
            helper[i++] = data[leftPtr] < data[rightPtr] ? data[leftPtr++] : data[rightPtr++];
        }
        while (leftPtr <= middle) {
            helper[i++] = data[leftPtr++];
        }
        while (rightPtr <= end) {
            helper[i++] = data[rightPtr++];
        }
        for (int j = 0; j < helper.length; j++) {
            data[start + j] = helper[j];
        }
        return result;
    }
}
