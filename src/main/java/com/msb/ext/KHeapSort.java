package com.msb.ext;

import com.msb.sort.AbstractSort;

import java.util.PriorityQueue;

/**
 * Copyright (C), 2024-06-15
 *
 * @author bza
 * @version v1.0.0
 * @create KHeapSort.java 2024-06-15 11:57
 */
public class KHeapSort<T extends Comparable<T>> extends AbstractSort<T> {

    private final int k;

    public KHeapSort(int k) {
        this.k = k;
    }

    @Override
    public void sort(T[] data) {
        if (data == null || data.length < 2) {
            return;
        }
        int i = 0;
        PriorityQueue<T> queue = new PriorityQueue<>(k + 1);
        for (int j = 0; j < data.length; j++) {
            queue.add(data[j]);
            if (queue.size() == k + 1 || j == data.length - 1) {
                data[i++] = queue.poll();
            }
        }
    }

}
