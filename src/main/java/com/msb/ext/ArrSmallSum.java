package com.msb.ext;

/**
 * Copyright (C), 2024-06-11
 *
 * @author bza
 * @version v1.0.0
 * @create ArrSmallSum.java 2024-06-11 20:33
 */
public class ArrSmallSum {


    public int process(Integer[] data) {
        return process(data, 0, data.length - 1);
    }

    private int process(Integer[] data, int start, int end) {
        if (start >= end) {
            return 0;
        }
        int middle = start + ((end - start) >> 1);
        int result = process(data, start, middle);
        result += process(data, middle + 1, end);
        result += merge(data, start, middle, end);
        return result;
    }


    private int merge(Integer[] data, int start, int middle, int end) {
        int leftPtr = start;
        int rightPtr = middle + 1;
        Integer[] helper = new Integer[end - start + 1];
        int i = 0;
        int result = 0;
        while (leftPtr <= middle && rightPtr <= end) {
            result += data[leftPtr] < data[rightPtr] ? data[leftPtr] * (end - rightPtr + 1) : 0;
            helper[i++] = data[leftPtr] < data[rightPtr] ? data[leftPtr++] : data[rightPtr++];
        }
        while (leftPtr <= middle) {
            helper[i++] = data[leftPtr++];
        }
        while (rightPtr <= end) {
            helper[i++] = data[rightPtr++];
        }
        for (int j = 0; j < helper.length; j++) {
            data[start + j] = helper[j];
        }
        return result;
    }
}
