package com.msb;

import com.msb.ext.LineSegmentCover;
import com.msb.model.LineSegment;
import com.msb.sort.RandomNumGenerator;
import com.msb.sort.SortAble;
import com.msb.sort.impl.*;
import org.junit.Test;

import java.util.Arrays;

public class TestSortAble {

    @Test
    public void testSelectionSort() {
        SortAble<Integer> sortAble = new SelectionSort<>();
        Integer[] data = RandomNumGenerator.generateNum(1, 150, 10);
        System.out.println(Arrays.toString(data));
        sortAble.sort(data);
        System.out.println(Arrays.toString(data));

        System.out.println("------------------------------------------------------------------------");

        Integer[] data2 = RandomNumGenerator.generateNum(1, 150, 10);
        System.out.println(Arrays.toString(data2));
        sortAble.sort(data2);
        System.out.println(Arrays.toString(data2));
    }

    @Test
    public void testBubbleSort() {
        SortAble<Integer> sortAble = new BubbleSort<>();

        Integer[] data = RandomNumGenerator.generateNum(1, 150, 10);
        System.out.println(Arrays.toString(data));
        sortAble.sort(data);
        System.out.println(Arrays.toString(data));

        Integer[] data2 = RandomNumGenerator.generateNum(1, 150, 10);
        System.out.println(Arrays.toString(data2));
        sortAble.sort(data2);
        System.out.println(Arrays.toString(data2));
    }

    @Test
    public void testInsertionSort() {
        SortAble<Integer> sortAble = new InsertionSort<>();

        Integer[] data = RandomNumGenerator.generateNum(1, 150, 12);
        System.out.println(Arrays.toString(data));
        sortAble.sort(data);
        System.out.println(Arrays.toString(data));

        System.out.println("------------------------------------------------------------------------");

        Integer[] data2 = RandomNumGenerator.generateNum(1, 150, 12);
        System.out.println(Arrays.toString(data2));
        sortAble.sort(data2);
        System.out.println(Arrays.toString(data2));
    }

    @Test
    public void testQuickSort() {
        SortAble<Integer> sortAble = new QuickSort<>();

        Integer[] data = RandomNumGenerator.generateNum(1, 150, 12);
        System.out.println(Arrays.toString(data));
        sortAble.sort(data);
        System.out.println(Arrays.toString(data));

        System.out.println("------------------------------------------------------------------------");

        Integer[] data2 = RandomNumGenerator.generateNum(1, 150, 12);
        System.out.println(Arrays.toString(data2));
        sortAble.sort(data2);
        System.out.println(Arrays.toString(data2));
    }

    @Test
    public void testShellSort() {
        SortAble<Integer> sortAble = new ShellSort<>();

        Integer[] data = RandomNumGenerator.generateNum(1, 150, 12);
        System.out.println(Arrays.toString(data));
        sortAble.sort(data);
        System.out.println(Arrays.toString(data));

        System.out.println("------------------------------------------------------------------------");

        Integer[] data2 = RandomNumGenerator.generateNum(1, 150, 12);
        System.out.println(Arrays.toString(data2));
        sortAble.sort(data2);
        System.out.println(Arrays.toString(data2));
    }

    @Test
    public void testMergeSort() {
        SortAble<Integer> sortAble = new MergeSort<>();

        Integer[] data = RandomNumGenerator.generateNum(1, 150, 12);
        System.out.println(Arrays.toString(data));
        sortAble.sort(data);
        System.out.println(Arrays.toString(data));

        System.out.println("------------------------------------------------------------------------");

        Integer[] data2 = RandomNumGenerator.generateNum(1, 150, 12);
        System.out.println(Arrays.toString(data2));
        sortAble.sort(data2);
        System.out.println(Arrays.toString(data2));
    }

    @Test
    public void testNonRecursiveMergeSort() {
        SortAble<Integer> sortAble = new NonRecursiveMergeSort<>();

        Integer[] data = RandomNumGenerator.generateNum(1, 150, 12);
        System.out.println(Arrays.toString(data));
        sortAble.sort(data);
        System.out.println(Arrays.toString(data));

        System.out.println("------------------------------------------------------------------------");

        Integer[] data2 = RandomNumGenerator.generateNum(1, 150, 12);
        System.out.println(Arrays.toString(data2));
        sortAble.sort(data2);
        System.out.println(Arrays.toString(data2));
    }

    @Test
    public void testQuickSortV2() {
        SortAble<Integer> sortAble = new QuickSortV2<>();

        Integer[] data = RandomNumGenerator.generateNum(1, 50, 10);
        System.out.println(Arrays.toString(data));
        sortAble.sort(data);
        System.out.println(Arrays.toString(data));
        System.out.println("------------------------------------------------------------------------");
        Integer[] data2 = RandomNumGenerator.generateNum(1, 30, 11);
        System.out.println(Arrays.toString(data2));
        sortAble.sort(data2);
        System.out.println(Arrays.toString(data2));
    }

    @Test
    public void testHeapSort() {
        SortAble<Integer> sortAble = new HeapSort<>();

        Integer[] data = RandomNumGenerator.generateNum(1, 50, 10);
        System.out.println(Arrays.toString(data));
        sortAble.sort(data);
        System.out.println(Arrays.toString(data));
        System.out.println("------------------------------------------------------------------------");
        Integer[] data2 = RandomNumGenerator.generateNum(1, 30, 11);
        System.out.println(Arrays.toString(data2));
        sortAble.sort(data2);
        System.out.println(Arrays.toString(data2));
    }



}
