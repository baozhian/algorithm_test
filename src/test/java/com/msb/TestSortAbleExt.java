package com.msb;

import com.msb.ext.*;
import com.msb.model.LineSegment;
import com.msb.sort.RandomNumGenerator;
import com.msb.sort.SortAble;
import com.msb.sort.impl.CountSort;
import com.msb.sort.impl.RadixSort;
import org.junit.Test;

import java.util.Arrays;

public class TestSortAbleExt {

    @Test
    public void testArrSmallSum() {
        ArrSmallSum sortAble = new ArrSmallSum();

        Integer[] data = RandomNumGenerator.generateNum(1, 10, 8);
        System.out.println(Arrays.toString(data));
        System.out.println(sortAble.process(data));

        System.out.println("------------------------------------------------------------------------");

        Integer[] data2 = RandomNumGenerator.generateNum(1, 10, 5);
        System.out.println(Arrays.toString(data2));
        System.out.println(sortAble.process(data2));
    }

    @Test
    public void testBigThenRightTwice() {
        BigThenRightTwice sortAble = new BigThenRightTwice();

        Integer[] data = RandomNumGenerator.generateNum(1, 15, 8);
        System.out.println(Arrays.toString(data));
        System.out.println(sortAble.process(data));
        System.out.println("------------------------------------------------------------------------");
        Integer[] data2 = RandomNumGenerator.generateNum(1, 10, 5);
        System.out.println(Arrays.toString(data2));
        System.out.println(sortAble.process(data2));
    }

    @Test
    public void testReversePair() {
        ReversePair sortAble = new ReversePair();

        Integer[] data = RandomNumGenerator.generateNum(1, 15, 8);
        System.out.println(Arrays.toString(data));
        System.out.println(sortAble.process(data));
        System.out.println("------------------------------------------------------------------------");
        Integer[] data2 = RandomNumGenerator.generateNum(1, 10, 5);
        System.out.println(Arrays.toString(data2));
        System.out.println(sortAble.process(data2));
    }

    @Test
    public void testKHeapSort() {
        SortAble<Integer> sortAble = new KHeapSort<>(2);

        Integer[] data = RandomNumGenerator.generateNum(1, 5, 3);
        Integer[] data2 = RandomNumGenerator.generateNum(6, 10, 3);
        Integer[] data3 = RandomNumGenerator.generateNum(11, 15, 3);

        Integer[] integers = new Integer[data.length + data2.length + data3.length];
        System.arraycopy(data, 0, integers, 0, data.length);
        System.arraycopy(data2, 0, integers, data.length, data2.length);
        System.arraycopy(data3, 0, integers, data.length + data2.length, data3.length);

        System.out.println(Arrays.toString(integers));
        sortAble.sort(integers);
        System.out.println(Arrays.toString(integers));
    }


    @Test
    public void testMaxCover() {
        LineSegment[] segments = LineSegment.getRandom(16);
        System.out.println(Arrays.toString(segments));
        int cover = LineSegmentCover.maxCover(segments);
        System.out.println(cover);
    }

    @Test
    public void testCountSort() {
        SortAble<Integer> sortAble = new CountSort();

        Integer[] integers = RandomNumGenerator.generateNum(1, 100, 15);
        System.out.println(Arrays.toString(integers));
        sortAble.sort(integers);
        System.out.println(Arrays.toString(integers));
    }

    @Test
    public void testRadixSort() {
        SortAble<Integer> sortAble = new RadixSort();

        Integer[] integers = RandomNumGenerator.generateNum(1, 250, 30);
        System.out.println(Arrays.toString(integers));
        sortAble.sort(integers);
        System.out.println(Arrays.toString(integers));
    }
}
